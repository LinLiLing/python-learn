'''
CSV文件操作
'''
import csv

## 9.4.1写入CSV文件

# 例9-17 将用户信息写入到CSV文件中
# '''
# 用户信息列表，嵌套列表内的每一个小列表是一行数据
datas = [["name", "age"],["zhangsan", 20],["lisi", 30]] #嵌套列表内的第一个列表是标题
with open("./user_info.csv", "w") as f:
    # writer函数会返回一个writer对象，通过writer对象向csv文件写数据
    writer = csv.writer(f)
    #循环遍历列表一次写入一行数据
    # for row in datas:
    #     writer.writerow(row)

    #一次写入多行
    writer.writerows(datas)
# '''

## 9.4.2读取CSV文件

# 例9-18 读取程序文件所在路径下的user_info.csv文件，查看用户信息
# with open("./user_info.csv","r",encoding="utf-8") as f:
#     reader = csv.reader(f) #返回一个reader可迭代对象
#     for row in reader:
#         print(row) #row是一个列表
#         print(row[0])#通过索引获取列表中元素
#         print(row[1])
#         print("---------------------")