'''
读写文件
'''
## 9.1.1写文件
#1.write方法

#例9-1 打开一个不存在的文件
'''
f = open("test.txt","r")#程序文件所在路径下没有test.txt文件
'''

#例9-2 使用只写模式打开一个不存在的文件
'''
f = open("test.txt","a")#程序文件所在路径下没有test.txt文件
'''

#例9-3 使用write方法向文件写入数据（w模式）
'''
f = open("stunames.txt","w")
#写入三个学生的名字
f.write("Tom")
f.write("David")
f.write("Carl")

#第二次写
# f.write("Frank")
# f.write("Harry")
'''

#例9-4 使用write方法向文件写入数据（a模式）
'''
f = open("stunames.txt","a")
f.write("Tom")
f.write("David")
f.write("Carl")
'''

#例9-5 使用write方法实现换行写
'''
f = open("stunames.txt","w")
f.write("Tom\n")
f.write("David\n")
f.write("Carl\n")
'''

#2.writelines方法

#例9-6 使用writelines方法一次写入多个字符串到文件中
'''
f = open("stunames.txt","w",encoding="utf-8")
f.writelines(["张三\n","李四\n","王五\n"])
'''

#3.close方法
#例9-7 使用with安全打开关闭文件
'''
with open("stunames.txt","w",encoding="utf-8") as f:
    f.writelines(["张三\n","李四\n","王五\n"])
'''

## 9.1.2读文件

#1.read方法

#例9-8 使用read方法读取stuname文件中所有学生名字
'''
with open("stunames.txt","r",encoding="utf-8") as f:
    data = f.read()
    print(data)
'''

#2.readlines方法

#例9-9 使用readlines方法按行读取stuname文件中所有学生名字
with open("stunames.txt","r",encoding="utf-8") as f:
    lines = f.readlines()
    print(lines)
    # 循环遍历lines，打印每行内容
    for i in range(0,len(lines)):
        print("第{}行：{}".format(i,lines[i]))


