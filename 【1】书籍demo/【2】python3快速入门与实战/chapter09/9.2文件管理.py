'''
文件管理
'''
import os

#1.rename函数
#例9-10 将文件“test.txt”重命名为“测试.txt”
# os.rename("test.txt","测试.txt")

#2.remove函数
#例9-11 删除当前路径下的“测试.txt”文件
# os.remove("测试.txt")

#5.listdir(path)函数
#例9-12 获取程序执行的当前路径下的文件列表
'''
lsdir = os.listdir("./")
print(lsdir)
'''
#6.rmdir(path)函数
#例9-13 删除程序执行的当前路径下的非空文件夹“datas”
os.rmdir("./datas")