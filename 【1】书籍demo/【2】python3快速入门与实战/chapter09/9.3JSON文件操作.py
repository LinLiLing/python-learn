'''
json文件操作
'''
import json


# 例9-14 定义一个存储用户信息的字典，
# 其中存储的数据格式是：姓名、年龄、编程语言(值是列表)、正在学习的课程（值是字典），是否是vip值是布尔类型，性别未知使用空类型None表示。
# 使用json模块的dumps函数将用户信息字典转换成JSON编码的字符串。

user_info_dict = {"name": "zhangsan",
             "age": 20,
             "language": ["python", "java"],
             "study": {"AI": "python", "bigdata": "hadoop"},
             "if_vip": True,
             "gender": None}
'''
json_str = json.dumps(user_info_dict)
print(json_str)
'''

# 例9-15 使用loads函数将json编码的字符串转换为Python数据结构
'''
python_dict = json.loads(json_str)
#转换之后的类型
print("类型为：{}".format(type(python_dict)))
#打印转换结果
print(python_dict)
'''

# 例9-16 使用dump函数将Python数据写入到JOSN文件
'''
with open("./user_info.json", "w") as f:
    json.dump(user_info_dict,f)
'''

# 例9-17 使用load函数加载JSON文件，并将JSON文件中的数据转换成Python数据结构
with open("./user_info.json","r") as f:
    user_info_dict = json.load(f)
    print(user_info_dict)