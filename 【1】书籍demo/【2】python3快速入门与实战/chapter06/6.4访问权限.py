'''
访问权限
'''
#例6-7 由于狗的年龄通常在1岁到20岁之间，为了防止外部对年龄属性随意修改设置非法年龄，
#      将Dog类的年龄属性设置为私有属性，通过内部私有方法设置年龄属性值
class Dog:
    def __init__(self,name):
        self.name = name
        self.__age = 1 #私有属性

    #私有方法，用于设置年龄
    def __set_age(self,age):
        self.__age = age

    #设置名字和年龄属性
    def set_info(self,name,age):
        #如果传入的名字不是空字符串，则给对象设置新的名字
        if name != "":
            self.name = name

        #合法年龄是1岁到20岁
        if age > 0 and age <= 20:
            #调用私有方法设置年龄
            self.__set_age(age)
        else:
            print("年龄设置失败，非法年龄！")

    def get_info(self):
        #在函数内访问私有属性"__age"
        print("我的名字是{}，我现在{}岁。".format(self.name, self.__age))

'''
wangcai = Dog("旺财")
wangcai.get_info()
#给旺财设置新的年龄
print("***我长大了***")
wangcai.set_info("",10)
wangcai.get_info()
'''
#例6-8 在类外部访问私有属性
wangcai = Dog("旺财")
print("我的名字是{}，我现在{}岁。".format(wangcai.name, wangcai.__age))




