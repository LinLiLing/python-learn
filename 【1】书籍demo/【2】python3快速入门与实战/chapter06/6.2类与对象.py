'''
面向对象：类与对象
'''
#例6-1 定义小狗类，小狗具备的行为是吃饭、喝水、奔跑
'''
class Dog:
    def eat(self):
        print("正在啃骨头...")
    def drink(self):
        print("正在喝水...")
    def run(self):
        print("摇着尾巴奔跑！")
'''

#例6-2 根据小狗类创建一个小狗对象旺财
'''
print("-------hello旺财-------")
wangcai = Dog()
#调用对象具备的行为（方法）
wangcai.eat()
wangcai.drink()
wangcai.run()
'''
print("-------hello托福-------")

#例6-3 再创建一个小狗对象托福
'''
tuofu = Dog()
#调用对象具备的行为（方法）
tuofu.eat()
tuofu.drink()
tuofu.run()

print("-------对比两只小狗的id--------")
print("wangcai的id：{}".format(id(wangcai)))
print("tuofu的id：{}".format(id(tuofu)))
'''

#例6-4 定义小狗类
class Dog:
    #构造方法
    def __init__(self):
        print("执行了构造方法，正在初始化...")
    def eat(self):
        print("正在啃骨头...")
    def drink(self):
        print("正在喝水...")
    def run(self):
        print("摇着尾巴奔跑！")

