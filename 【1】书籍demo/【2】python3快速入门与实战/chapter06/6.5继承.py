'''
继承
'''
## 6.5.1单继承
'''
#例6-9 狗类继承动物类
#定义Animal类
class Animal:
    #定义动物类具备的一些行为
    def eat(self):
        print("-----吃-----")
    def drink(self):
        print("-----喝-----")
    def run(self):
        print("-----跑-----")

#定义子类Dog继承父类Animal
class Dog(Animal):
    #小狗除了继承父类Animal具备的行为，它还会握手
    def hand(self):
        print("*****握手*****")

wangcai = Dog()
#子类Dog中没有定义eat、drink、run方法
wangcai.eat()
wangcai.drink()
wangcai.run()
wangcai.hand()

#例6-10 金毛类继承狗类
#定义子类GoldenDog金毛继承父类Dog
class GoldenDog(Dog):
    #金毛能够作为导盲犬，具被导航功能
    def guide(self):
        print("+++++我能导航+++++")

duoduo = GoldenDog()
duoduo.guide()
duoduo.hand()#调用父类Dog的hand方法
duoduo.eat() #父类Dog的父类Animal的eat方法
'''

#例6-11 继承中构造方法的使用
'''
class Animal:
    #定义构造方法
    def __init__(self):
        print("*****Animal初始化*****")
    def eat(self):
        print("-----吃-----")
    def drink(self):
        print("-----喝-----")
    def run(self):
        print("-----跑-----")

class Dog(Animal):
    #定义构造方法
    def __init__(self):
        print("*****Dog初始化*****")
    def hand(self):
        print("******握手*******")

class GoldenDog(Dog,Animal):
    def guide(self):
        print("+++++我能导航+++++")

#在GoldenDog中没有定义构造方法，创建 GoldenDog对象，会自动调用父类Dog的init构造方法
duoduo = GoldenDog()
duoduo.guide()

wangcai = Dog()
wangcai.hand()
'''

## 6.5.2super()

#例6-12 子类GoldenDog通过“父类名.方法名(self[,参数列表])”方式调用父类Dog的hand方法
class Animal:
    def __init__(self):
        print("*****Animal初始化*****")
    def eat(self):
        print("-----吃-----")
    def drink(self):
        print("-----喝-----")
    def run(self):
        print("-----跑-----")

class Dog(Animal):
    def __init__(self):
        print("*****Dog初始化*****")
    def hand(self):
        print("*****握手*****")
'''
class GoldenDog(Dog):
    def guide(self):
        print(">>>>>我能导航<<<<<")
        print("提示：想让我导航，请先跟我握手！")
        #通过"父类名.方法名"调用父类Dog的hand方法
        Dog.hand(self)
'''
#例6-13 子类GoldenDog通过“super().方法名”的方式调用父类Dog的hand方法
class GoldenDog(Dog):
    def guide(self):
        print(">>>>>我能导航<<<<<")
        print("提示：想让我导航，请先跟我握手！")
        #通过"super().方法名"调用父类Dog的hand方法
        super().hand()

duoduo = GoldenDog()
duoduo.guide()


## 6.5.3重写
#例6-14 子类Dog重写父类Animal的run方法
'''
class Animal:
    def __init__(self):
        print("*****Animal初始化*****")
    def eat(self):
        print("-----吃-----")
    def drink(self):
        print("-----喝-----")
    def run(self):
        print("-----跑-----")

class Dog(Animal):
    def __init__(self,name):
        self.name = name
        print("我的名字是：{}".format(self.name))
    #重写父类run方法
    def run(self):
        print("摇摆着尾巴跑！")

class Cat(Animal):
    def __init__(self,name):
        self.name = name
        print("我的名字是：{}".format(self.name))

animal = Animal()
animal.run()

wangcai = Dog("旺财")
wangcai.run() #调用了Dog类重写父类的run方法

tom = Cat("Tom")
tom.run() #调用了继承自父类Animal类的run方法
'''

## 6.5.4多继承
#例6-15 大数据和人工智能是两个不同的领域，那我们现在学习的Python，既可以做大数据领域相关的工作，也可以做人工智能领域相关的工作。
#那么我们抽象出三个类，人工智能类AI，大数据类BigData，Python语言类PythonLanguage，PythonLanguage类继承AI类和BigData类
'''
#人工智能类
class AI:
    #人脸识别
    def face_recognition(self):
        print("---人脸识别---")

#大数据类
class BigData():
    #数据分析
    def data_analysis(self):
        print("***数据分析***")

#Python语言类继承AI和BigData
class PythonLanguage(AI,BigData):#多继承
    def operation(self):
        print("+++运维+++")

py = PythonLanguage()
#分别调用两个父类的方法
py.data_analysis()
py.face_recognition()
py.operation()
'''
#例6-16 在AI和BigData两个领域，数据处理是都做的工作，在AI和BigData两个类中定义数据处理方法
'''
#人工智能类
class AI:
    #人脸识别
    def face_recognition(self):
        print("---人脸识别---")

    #数据处理
    def data_handle(self):
        print("AI数据处理")
#大数据类
class BigData():
    #数据分析
    def data_analysis(self):
        print("***数据分析***")

    #数据处理
    def data_handle(self):
        print("BigData数据处理")

#Python语言类继承AI和BigData
class PythonLanguage(AI,BigData):#多继承
    def operation(self):
        print("+++运维+++")

py = PythonLanguage()
#通过类内置的__mro__属性可以查看方法解析（搜索）顺序
print(PythonLanguage.__mro__)
#调用不同父类中同名的方法
py.data_handle()
'''