import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

# s1 = pd.Series(np.random.randn(100))
# s1.plot()
# plt.show()


# s1 = pd.Series(np.random.randn(100))
# s1.plot(style = 'ko--', alpha = 0.5, grid = True, label = 's1',
#         title= 'Plot for 100 Random Data from Standard Normal Distribution')
# plt.legend()
# plt.show()

# s2 = pd.Series(np.random.randn(100))
# s3 = pd.Series(np.random.randn(100))
# s2.plot(label = 's1')
# s3.plot(label = 's2',style = '--')
# plt.legend()
# plt.show()

# DataFrame
# df = pd.DataFrame(np.random.randn(10,3).cumsum(0),columns=['One','Two','Three'])
# df.plot()
# plt.show()

# 柱状图
# df = pd.DataFrame(np.random.randn(10,3),columns=['One','Two','Three'])
# df.plot(kind = 'bar')
# plt.show()

# 堆叠柱状图
df = pd.DataFrame(np.random.randn(10,3),columns=['One','Two','Three'])
df.plot(kind = 'bar',stacked = True)
plt.show()