from turtle import color
from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
from pyrsistent import plist

## 1000对正态分布随机数散点图1
# x = np.random.randn(1000)
# y = np.random.randn(1000)
# plt.scatter(x, y,c='b',marker="*",alpha=0.5)  # c = color , 'y' = yellow ,可以是一个数值也可以是一维数组。 's':方形，'p':五边形，'*':星形
# plt.title('Scatter plot for 1000 random data from normal distribution',loc= 'right')
# plt.xlim(-5,5)
# plt.ylim(-5,5)
# plt.show()

# ## 1000对正态分布随机数散点图2
# x = np.random.randn(1000)
# y = np.random.randn(1000)
# plt.scatter(x, y, color = 'g', marker = '*', alpha = 0.5)
# plt.title('Scatter plot for 1000 random data from normal distribution')
# plt.show()

# ## 调整坐标轴范围之后1000对正态分布随机数散点图
# x = np.random.randn(1000)
# y = np.random.randn(1000)
# plt.scatter(x, y, color = 'g', marker = '*', alpha = 0.5)
# plt.title('Scatter plot for 1000 random data from normal distribution')
# plt.xlim(-5,5)
# plt.ylim(-5,5)
# plt.show()

# 折线图
# growth = pd.read_table('growth.txt',sep = ' ')
# plt.plot(growth['Year'], growth['Sweden'], color = 'r', label = 'Sweden',linestyle = '--')
# plt.plot(growth['Year'], growth['Norway'], color = 'pink', label = 'Norway')
# plt.plot(growth['Year'], growth['New Zealand'], color = 'purple', label = 'New Zealand',marker = '+')
# plt.title('Real GDP Growth for Three Countries between 1946 and 2009')
# plt.xlabel('Year')
# plt.ylabel('Real GDP Growth')
# plt.legend()
# plt.show()
# growth.plot(kind= 'bar')

#柱状图
####plt.bar(growth.columns.tolist()[1:], growth.iloc[11][1:])
index = np.arange(5)
value = [20,30,45,35,50]
plt.bar(index, value, width = 0.5, label = 'population', facecolor = '#ff0000')
plt.title('Grouping Allocation')
plt.xticks(index, ('Group1','Group2','Group3','Group4','Group5'))
plt.yticks(np.arange(0,60,10))
plt.legend(loc='upper left')
a = zip(index,value)
for x,y in a:
    plt.text(x, y, y, ha='center', va='bottom')
plt.show()

goals = pd.read_csv('WorldCupMatches.csv')
home_team_goals = goals['Home Team Goals']
away_team_goals = goals['Away Team Goals']

plt.boxplot((home_team_goals,away_team_goals), labels = ['Home Teams','Away Teams'])
plt.title('Goals for Home Teams and Away Teams in all FIFA World Cups')
plt.show()



