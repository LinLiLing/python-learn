import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

tips = sns.load_dataset('tips')
print(tips.head())
#
# 单变量条形图
# sns.stripplot(x = 'day', y = 'total_bill', data = tips)
# plt.show()
#
# 单变量条形图非重叠
# sns.stripplot(x = 'day', y = 'total_bill', data = tips, jitter = True)
# plt.show()
#
# 横向单变量条形散点图
# sns.stripplot(x = 'total_bill', y = 'size', data = tips, jitter = True, orient= 'h')
# plt.show()
#
# 双变量条形散点图
# sns.swarmplot(x = 'day', y = 'total_bill', data = tips, hue = 'sex')
# plt.show()

# 箱线图
# sns.boxplot(x = 'day', y = 'total_bill', hue = 'smoker',data = tips, palette = 'Reds')
# plt.show()

# 琴形图
# sns.violinplot(x = 'day', y = 'total_bill', data = tips)
# plt.show()

# 分类琴形图
# sns.violinplot(x = 'day', y = 'total_bill', hue = 'smoker', palette= 'coolwarm', inner = 'quartile', split= True, data = tips)
# plt.show()

# 小提琴图结合散点图
# sns.violinplot(x = 'day', y = 'total_bill', data = tips, palette = 'hls', inner = None)
# sns.swarmplot(x = 'day',  y = 'total_bill', data = tips, color = 'w', alpha = 0.5)
# plt.show()

# factorplot
# 点图
# sns.factorplot(x='day', y='total_bill', hue='smoker', data=tips)
# plt.show()

# 柱形图
# sns.factorplot(x='day', y='total_bill', hue= 'smoker', kind = 'bar', data=tips)
# plt.show()

# 条形散点图
# sns.factorplot(x='day', y='total_bill', hue='smoker',
#                col='time', kind = 'swarm', data=tips)
# plt.show()

# 箱线图
# sns.factorplot(x='day', y='total_bill', hue='smoker',
#                col='time', kind = 'box', data=tips)
# plt.show()

# 多变量琴形图
g = sns.PairGrid(tips,
                 x_vars=['smoker', 'time', 'sex'],
                 y_vars=['total_bill', 'tip'],
                 aspect=.75, size=3.5)
g.map(sns.violinplot, palette="hls")
plt.show()