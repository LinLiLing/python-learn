'''
raise抛出异常
'''
#例7-9 除法运算函数，当除数为0抛出ZeroDivisionError异常
'''
def div(a,b):
    if b == 0:
        raise ZeroDivisionError
    else:
        return a / b
'''
#例7-10 给ZeroDivisionError异常传入参数
def div(a,b):
    if b == 0:
        raise ZeroDivisionError("异常原因：除法运算，除数不能为0！")
    else:
        return a / b

div(2,0)
