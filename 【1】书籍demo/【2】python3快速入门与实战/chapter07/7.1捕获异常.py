'''
捕获异常
'''
#例7-1 打开一个不存在的文件，产生异常
'''
file = open("test.txt")
print(file.read())
file.close()
print("文件读取结束！")
'''
#例7-2 捕获异常
'''
print("准备打开一个文件...")
try:
    open("test.txt")
    print("打开文件成功！")
except FileNotFoundError as err:
    print("捕获到了异常，文件不存在！",err)

print("程序即将结束！")
'''

