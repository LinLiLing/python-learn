'''
finally语句
'''
#例7-5 产生了异常，并且捕获了异常，执行finally语句
'''
try:
    f = open("test.txt")
    print("打印文件内容")
except FileNotFoundError as err:
    print("捕获到了异常！",err)
finally:
    print("关闭连接！")
'''

#例7-6 产生了异常，没有捕获异常，执行finally语句
'''
try:
    f = open("test.txt")
    print("打印文件内容")
finally:
    print("关闭连接！")
'''

#例7-7 没有异常，执行finally语句
try:
    num = "hello python!"
    print(num)
finally:
    print("关闭连接！")

