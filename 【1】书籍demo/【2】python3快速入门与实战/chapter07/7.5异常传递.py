'''
异常传递
'''
#例7-8 异常传递
'''
def func1():
    print("------test1-1------")
    # 打印一个不存在的变量，产生异常，但是没有捕获
    print(num)
    print("------test1-2------")
def func2():
    try:
        print("------test2-1------")
        # 调用func1函数，捕获异常
        func1()
        print("------test2-2------")
    except Exception as err:
        print("捕获到了异常：",err)
        print("------test2-3------")

func2()
'''