'''
捕获多个异常
'''
#例7-3 捕获多个异常
'''
try:
    # 打印一个不存在的变量，报NameError异常
    print(num)
    # 读取一个不存在的文件，报FileNotFoundError异常
    open("test.txt")
except (NameError,FileNotFoundError) as err:
    print("捕获到了异常！",err)

print("程序即将结束！")
'''

