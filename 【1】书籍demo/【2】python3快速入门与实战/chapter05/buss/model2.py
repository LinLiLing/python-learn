'''
#引入模块，通过包名.模块名的方式引入
import buss.model1
import tool.model1

#分别调用两个同名模块中的不同函数
buss.model1.project_info()
tool.model1.tool_info()
'''

'''
#同时引入多个模块
import buss.model1,tool.model1
buss.model1.project_info()
tool.model1.tool_info()
'''

from buss.model1 import project_info
project_info()