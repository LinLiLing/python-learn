'''
字符串的定义与使用方法
'''

'''
#例2-19 拼接字符串
new_str = "hello" + "=>" + "python" + str(3)
print(new_str)

#例2-20 字符串格式化
name = "小明"
print("姓名：%s"%name)
age = 20
print("年龄：%d岁"%age)
high = 180.5
print("身高：%f厘米"%high)

#例2-21 格式化字符串传入多个变量值
name = "小明"
age = 20
high = 180.5
print("姓名：%s，年龄：%d岁，身高：%f厘米"%(name,age,high))

#例2-22 设置浮点数精度值
print("姓名：%s，年龄：%d岁，身高：%.1f厘米"%(name,age,high))

#例2-23 format函数的使用
print("姓名：{}，年龄：{}岁，身高：{:.1f}厘米".format(name,age,high))
'''

## 内置方法
#例2-24 find()方法的使用
line = "hello world hello python"
'''
#查找字符串中的world
print(line.find("world"))
#line中包含两个hello，只返回第一个hello的起始下标
print(line.find("hello"))
#从下标6开始向后查找hello
print(line.find("hello",6))
#如果没有找到子字符串则返回-1
print(line.find("zhangsan"))
'''

#例2-25 count()方法的使用
'''
#统计line中包含的hello的个数
print("line中包含的hello个数：",line.count("hello"))
'''
#例2-26 replace()方法的使用
'''
#将line中的所有hello替换成hi
new_line = line.replace("hello","hi")
print("     line:",line)
print(" new_line:",new_line)
#只替换1个子字符串
new_line2 = line.replace("hello","hi",1)
print("     line:",line)
print("new_line2:",new_line2)
'''
#例2-27 split()方法的使用
#按照空格分割字符串
'''
split_list = line.split(" ")
print("line:",line)
print("split:",split_list)
#按照空格分割字符串，只对匹配的一个分隔符进行分割
split_list2 = line.split(" ",1)
print("line:",line)
print("split2:",split_list2)
'''
##例2-28 startswith 与endswith方法的使用
'''
#判断line字符串是否以hello开头
print(line.startswith("hello"))
#判断line字符串是否以python结尾
print(line.endswith("python"))
'''

#例2-29 lower()与upper()方法的使用
'''
content = input("提示信息，取款成功，yes继续，no直接退出：")
print("您输入的信息：",content)
#为了防止用户输入大写字符串，影响后续的判断，使用lower将用户输入的内容全部转换为小写
if content.lower() == "yes":
    print("欢迎继续使用！")
else:
    print("退出成功，请取卡！")
'''

#例2-30 join()方法
'''
#姓名、年龄、所在城市这些信息用逗号隔开拼接成一个字符串
print(",".join(["小明",str(20),"北京"]))
'''
#例2-31 strip()方法的使用
#默认去掉字符串头和尾的空白字符
#去掉空格
print("  sss  ".strip())
#去掉制表符
print("\tsss\t".strip())
#去掉换行符
print("\nsss\n".strip())
#指定的字符作为参数，去掉字符串头和为的指定字符
print(",,,,sss,,,,".strip(","))