'''
变量的使用方法
'''
#例2-3 打印个人信息
#定义姓名变量name
name = "xiaoming"
#定义身高变量high
high = 180.5
#定义年龄变量age
age = 20
#打印个人信息相关的变量值
print("姓名：",name)
print("身高：",high)
print("年龄：",age)