'''
注释的使用方法
'''
##单行注释
#例2-1 在代码中添加单行注释
#下面代码实现的功能是：打印一行文字
print("hello world")
#print("hello python") #注释一行代码

##多行注释
#例2-2 在代码中添加多行注释
'''
使用多行注释描述代码功能
打印一行文字hello world
'''
print("hello world")