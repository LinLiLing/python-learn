'''
数据类型：数字类型
'''
#例2-6 整型变量
'''
a = 10
b = 20
c = a + b
print("变量a类型：",type(a))
print("变量b类型：",type(b))
print("变量c类型：",type(c))
'''
#例2-7 计算圆面积
'''
pi = 3.14
r = 4
area = pi * r ** 2
print("圆形面积：",area)
print("变量pi类型：",type(pi))
print("变量r类型：",type(r))
print("变量area类型：",type(area))
'''

#例2-8 类型转换函数使用
'''
a1= int("123") #字符串转整型
a2= int(3.14) #浮点型转整型
f1= float(3) #整型转浮点型
f2= float("3.14") #字符串转浮点型
s1= str(123) #整型转字符串
s2= str(3.14) #浮点型转字符串

print("a1类型：",type(a1)," a1的值：",a1)
print("a2类型：",type(a2)," a2的值：",a2)
print("f1类型：",type(f1)," f1的值：",f1)
print("f2类型：",type(f2)," f2的值：",f2)
print("s1类型：",type(s1)," s1的值：",s1)
print("s2类型：",type(s2)," s2的值：",s2)
'''

#例2-9 布尔类型变量
t = True
f = False
print(type(t))
print(type(f))