'''
input输入与print输出
'''
#例2-10 对用户输入的两个数字求和
'''
#接收用户输入数字,input函数返回字符串类型信息
num1 = input("请输入第一个数字:")
num2 = input("请输入第二个数字:")
#使用type函数查看变量类型
print(type(num1))
print(type(num2))
#使用类型转换函数将字符串类型数字转换成整型
num1_1 = int(num1)
num1_2 = int(num2)
result = num1_1 + num1_2
print("result:",result)
'''
## print输出函数

#例2-11 打印变量值
'''
print("hello python")
str = "hello world,hello python!"
#打印变量值
print(str)
a = 10
b = 20
#同时打印多个变量值
print(a,b)
'''
#例2-12 无换行打印
'''
print("hello",end="")
print(" python",end="")
print(" !",end="")
'''
#例2-13 转义字符
print("中国\n北京")
print("-------华丽分割线-----")
print("中国\\n北京")


