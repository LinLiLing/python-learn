'''
for循环
'''

#例2-39 for循环的使用，打印0到9十个数字
'''
for i in range(0,10):
    print(i)
'''

#break跳出整个循环

#例2-40 打印1到20的偶数，当遇到10的整数倍数字时，结束整个循环
'''
for i in range(1,21):
    if i % 2 == 0:
        if i % 10 == 0:
            break
        print(i)
'''
#continue跳出当次循环

#例2-41 打印1到20的偶数，不打印10的整数倍偶数
for i in range(1,21):
    if i % 2 == 0:
        if i % 10 == 0:
            continue
        print(i)