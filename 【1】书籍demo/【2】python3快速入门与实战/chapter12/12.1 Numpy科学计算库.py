import numpy as np

a = [1.1,2.2,3.3,4.4]
arr = np.array(a)
print('a:{}'.format(a))
print('arr:{}'.format(arr))

b = [[1,2,3,4],[5,6,7,8]]
arr1 = np.array(b)
print('arr1:{}'.format(arr1))

print('arr shape:{}'.format(arr.shape))
print('arr1 shape:{}'.format(arr1.shape))

# arr1.shape = 4,2
# print(arr1)

arr1_reshape = arr1.reshape(4,2)
print('arr1_reshape: {}'.format(arr1_reshape))
print('arr1: {}'.format(arr1))

arr1[0,0] = 10
print('arr1: {}'.format(arr1))
print('arr1_reshape: {}'.format(arr1_reshape))


print('arr dtype: {}'.format(arr.dtype))
print('arr1 dtype:{}'.format(arr1.dtype))

arr2 = np.array([1,2,3,4,5], dtype=np.int32)
arr3 = np.array([1,2,3,4,5], dtype=np.float64)
print('arr2: {},  arr2 dtype: {} '.format(arr2,arr2.dtype))
print('arr3: {},  arr3 dtype: {} '.format(arr3,arr3.dtype))

arr2_float = arr2.astype(np.float32)
print('arr2_float: {},  arr2_float dtype: {} '.format(arr2_float,arr2_float.dtype))

arr3_int = arr3.astype(np.int32)
print('arr3_int: {},  arr3_int dtype: {} '.format(arr3_int,arr3_int.dtype))

# 转换数据类型
arr4_string = np.array(['1.1','2.2','3.3','4.4'],dtype=np.string_)
arr4_float = arr4_string.astype(np.float64)
print('arr4_float: {}, arr4_float dtype: {} '.format(arr4_float, arr4_float.dtype))

# 创建0数组 1数组 空数组 单位数组
print('zero_array1: {}'.format(np.zeros(5)))
print('zero_array2: {}'.format(np.zeros((3,6))))
print('one_array: {}'.format(np.ones((2,3))))
print('empty_array: {}'.format(np.empty((2,2,4))))
print('arange_array: {}'.format(np.arange(10)))
print('eye_array: {}'.format(np.eye(3,3)))

# 一维数组索引
arr5 = np.arange(15)
print('arr5: {}'.format(arr5))
print('arr5[8]: {}'.format(arr5[8]))
print('arr5[8:12]: {}'.format(arr5[8:12]))

# 数组切片
arr5[8:12] = 30
print('arr5: {}'.format(arr5))

# 复制修改数组
arr5 = np.arange(15)
arr5_origin = arr5.copy()
arr5[8:12] = 30
print('arr5: {}'.format(arr5))
print('arr5_origin: {}'.format(arr5_origin))

# 二维数组索引
arr_2d = np.array([[1,1,1],[2,2,2],[3,3,3]])
print('arr_2d[1]: {}'.format(arr_2d[1]))

print('arr_2d[2,1]: {}'.format(arr_2d[2,1]))
print('arr_2d[2][1]: {}'.format(arr_2d[2][1]))

# 多维数组索引
# 创建一个三维数组
arr_3d = np.arange(16).reshape((2,2,4))
print('arr_3d:{}'.format(arr_3d))

print('arr_3d[0]: {}'.format(arr_3d[0]))
print('arr_3d[1]: {}'.format(arr_3d[1]))
print('arr_3d[0]: {}'.format(arr_3d[0,1]))

arr_3d_origin = arr_3d[0].copy()
arr_3d[0] = 50
print('arr_3d: {}'.format(arr_3d))
arr_3d[0] = arr_3d_origin
print('arr_3d: {}'.format(arr_3d))


arr6 = np.arange(25).reshape(5,5)
print('arr6: {}'.format(arr6))
print('arr6[1,2:4]: {}'.format(arr6[1,2:4]))
print('arr6[1:3,2:4]: {}'.format(arr6[1:3,2:4]))
print('arr6[:,2:]: {}'.format(arr6[:,2:]))
print('arr6[::2,2:]: {}'.format(arr6[::2,2:]))

x = np.array([0,1,2,3,1])
print(x==1)

print('arr6[x==1]: {}'.format(arr6[x==1]))
print('arr6[x==1, 3:]: {}'.format(arr6[x==1, 3:]))

# 反向索引
print('arr6[x!=1]: {}'.format(arr6[x!=1]))
print('arr6[~(x==1)]: {}'.format(arr6[~(x==1)]))
print('arr6[np.logical_not(x==1)]: {}'.format(arr6[np.logical_not(x==1)]))

# 多条件
print('arr6[(x==1) | (x==0)]: {}'.format(arr6[(x==1) | (x==0)]))

arr6[arr6>10] = 10
print('arr6: {}'.format(arr6))

arr6 = np.arange(25).reshape(5,5)
print('arr6.sum(): {}'.format(arr6.sum()))
print('arr6.mean(): {}'.format(arr6.mean()))
print('arr6.std(): {}'.format(arr6.std()))
print('arr6.var(): {}'.format(arr6.var()))
print('arr6.max(): {}'.format(arr6.max()))
print('arr6.min(): {}'.format(arr6.min()))
print('arr6.cumsum(): {}'.format(arr6.cumsum()))
print('arr6.cumprod(): {}'.format(arr6.cumprod()))
print('arr6.argmin(): {}'.format(arr6.argmin()))
print('arr6.argmax(): {}'.format(arr6.argmax()))

arr7 = np.array([[1,2,3],[4,5,6],[7,8,9]])
print('arr7: {}'.format(arr7))
print('arr7.T: {}'.format(arr7.T))
print('arr7: {}'.format(np.linalg.inv(arr7)))
arr8 = np.array([[4,5,6],[7,8,9],[1,2,4]])
print('arr7+arr8: {}'.format(arr7+arr8))
print('arr7-arr8: {}'.format(arr7-arr8))
#矩阵的点乘
print('矩阵arr7的点乘: {}'.format(arr7*arr8))
# 矩阵的乘法
print('arr7乘arr8: {}'.format(arr7.dot(arr8.T)))
# 矩阵的迹
print('arr7的迹: {}'.format(np.trace(arr7)))

eigvalue,eigvector = np.linalg.eig(arr7)
print('特征值为： {}，'.format(eigvalue))
print('特征向量为： {}'.format(eigvector))

arr9 = np.array([[2,4,3],[5,4,2],[9,0,3]])
arr9.sort(axis=1)
print('arr9: {}'.format(arr9))
print('按行排序后的矩阵：{}'.format(arr9))
arr10 = np.array([[2,4,3],[5,4,2],[9,0,3]])
np.sort(arr10,axis=0)
print('按列排序后得矩阵：{}'.format(arr10))

arr11 = np.array([3,2,1,4,3,1,2,4,2,3])
print('去重后的元素：{}'.format(np.unique(arr11)))

arr12 = np.arange(9).reshape(3,3)
np.savetxt('arr12.txt',arr12)

a = np.loadtxt('arr12.txt',delimiter=' ')
print('文件内容: {}'.format(a))