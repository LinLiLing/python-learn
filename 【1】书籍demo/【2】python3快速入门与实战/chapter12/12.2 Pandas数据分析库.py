# -*- coding:UTF-8 -*-
import pandas as pd
import numpy as np
### Series
# s1 = pd.Series([1,2,3,4,5])
# print('s1: {}'.format(s1))
#
# s2 = pd.Series([1,2,3,4,5],index=['第一','第二','第三','第四','第五'])
# print('s2: {}'.format(s2))
#
# print('s2索引: {}'.format(s2.index))
# print('s2值: {}'.format(s2.values))
#
# print('s2中 第二 对应的数值： {}'.format(s2['第二']))
# s2['第二'] = 10
# print('s2中 第二第四第五 对应的数值： {}'.format(s2[['第二','第四','第五']]))
# print('s2中 第二到第五 对应的数值： {}'.format(s2['第二':'第五']))
#
# s_dic = {'First':1, 'Second':2, 'Third':3, 'Fourth':4, 'Fifth':5}
# s3 = pd.Series(s_dic)
# print('s3: {}'.format(s3))
#
# s_dic = {'First':1, 'Second':2, 'Third':3, 'Fourth':4, 'Fifth':5}
# s4 = pd.Series(s_dic,index=['First','Second','Third','Fourth','Fifth'])
# print('s4: {}'.format(s4))
#
# print('s4 中含有sixth: {}'.format('sixth' in s4))
# print('s4 中不含有sixth: {}'.format('sixth' not in s4))
#
# s_dic = {'First':1, 'Second':2, 'Third':3, 'Fourth':4, 'Fifth':5}
# s5 = pd.Series(s_dic,index=['First','Second','Third','Fourth','Tenth'])
# print('s5: {}'.format(s5))
#
# print('数据缺失：{}'.format(s5.isnull()))
# print('数据不缺失：{}'.format(s5.notnull()))
#
# print('s3+s5: {}'.format(s4+s5))

### DataFrame
# df_dic = {'color':['red','yellow','blue','purple','pink'],
#           'size':['medium','small','big','medium','small'],
#           'taste':['sweet','sour','salty','sweet','spicy']}
# df = pd.DataFrame(df_dic)
# print('df: {}'.format(df))
#
# # 指定列排序
# df_dic = {'color':['red','yellow','blue','purple','pink'],
#           'size':['medium','small','big','medium','small'],
#           'taste':['sweet','sour','salty','sweet','spicy']}
# df1 = pd.DataFrame(df_dic, columns=['taste','color','size'])
# print('df1: {}'.format(df1))
#
#
# df_dic = {'color':['red','yellow','blue','purple','pink'],
#           'size':['medium','small','big','medium','small'],
#           'taste':['sweet','sour','salty','sweet','spicy']}
# df2 = pd.DataFrame(df_dic, columns=['taste','color','size','category'])
# print('df2: {}'.format(df2))
#
# df2.index.name = 'sample'
# df2.columns.name = 'feature'
# print('df2: {}'.format(df2))
#
# print('df2的values值为： {}'.format(df2.values))
#
# print('df2中的color列： {}'.format(df2['color']))
# print('df2中的color列： {}'.format(df2.color))
#
# print('df2中行序号为3： {}'.format(df2.ix[3]))
#
# df2['category'] = np.arange(5)
# print('df2: {}'.format(df2))
#
# df_dic = {'color':['red','yellow','blue','purple','pink'],
#           'size':['medium','small','big','medium','small'],
#           'taste':['sweet','sour','salty','sweet','spicy']}
# df2 = pd.DataFrame(df_dic, columns=['taste','color','size','category'])
# df2['category'] = pd.Series([2,3,4],index=[0,2,4])
# print('df2: {}'.format(df2))
#
# df2['country'] = pd.Series(['China','UK','USA','Australia','Japan'])
# print('df2: {}'.format(df2))
#
# print('df2中category小于等于3的样本数据： {}'.format( df2[ df2['category']<=3 ] ) )

df3 = pd.DataFrame([[3,2,3,1],[2,5,3,6],[3,4,5,2],[9,5,3,1]],
                  index=['a','b','c','d'],columns=['one','two','three','four'])
print('df3: {}'.format(df3))

print('df3.sum 按列求和: {}'.format(df3.sum()))
print('df3.sum 按行求和: {}'.format(df3.sum(axis = 1)))

print('df3.sum 从上到下累计求和: {}'.format(df3.cumsum()))
print('df3.sum 从左到右累计求和: {}'.format(df3.cumsum(axis = 1)))

df4 = pd.DataFrame([[3,np.nan,3,1],[2,5,np.nan,6],[3,4,5,np.nan],[5,3,1,3]],
                  index=['a','b','c','d'],columns=['one','two','three','four'])
# 输出判断dataframe中每个位置是否是缺失值
print(df4.isnull())

# 输出含有缺失值的行
print(df4[df4.isnull().any(axis=1)])

# 创建一个Series数组
arr = pd.Series([1,2,3,np.nan,5,6])
print('arr: {}'.format(arr))
# 过滤缺失值
print('过滤缺失值：{}'.format(arr.dropna()))
print('过滤缺失值之后的arr: {}'.format(arr))

arr = arr.dropna()
arr.dropna(inplace=True)
print('过滤缺失值之后的arr: {}'.format(arr))

# 输出df4过滤缺失值之后的结果
print(df4.dropna())

df4['fifth'] = np.NAN
# 输出df4
print('df4: {}'.format(df4))
# 输出过滤缺失值之后的结果
print('过滤缺失值之后： {}'.format(df4.dropna(how = 'all',axis=1,inplace=True)))

# 输出df4
print(df4)
# 输出用0替换缺失值之后的df4
print(df4.fillna(0))

# 输出用中位数替换缺失值之后的df4
print(df4.fillna(df4.median()))

# 输出向上填充之后的df4
print(df4.ffill())
# 输出向下填充之后的df4
print(df4.bfill())

df4 = pd.DataFrame([[3,5,3,1],[2,5,5,6],[3,4,5,3],[5,3,1,3],[3,4,5,3],[3,4,6,8]],
                  index=['a','b','c','d','e','f'],columns=['one','two','three','four'])
# 查看是否存在重复行
print(df4[df4.duplicated()])
# 查看是否存在前两列重复的行
print(df4[df4.duplicated(subset=['one','two'])])

# 删除重复列，保留第一次出现的重复行
print(df4.drop_duplicates(subset=['one','two'],keep='first'))

df5 = pd.DataFrame([[3,3,2,4],[5,4,3,3]],
                  index=['g','h'],columns=['one','two','three','four'])
# 输出合并之后的DataFrame
print(df4.append(df5))

# df4和df5上下连接
print(pd.concat([df4,df5]))

# df4和df5左右连接
print(pd.concat([df4,df5],axis=1))



df_dic = {'color':['red','yellow','blue','purple','pink'],
           'size':['medium','small','big','medium','small'],
           'taste':['sweet','sour','salty','sweet','spicy'],
          'category':[2,3,4,5,6]}
df6 = pd.DataFrame(df_dic, columns=['taste','color','size','category'])
print('df6:{}'.format(df6))
df_dic1 = {'country':['China','UK','USA','Australia','Japan'],
           'quality':['good','normal','excellent','good','bad'],
          'category':[2,3,5,6,7]}
df7 = pd.DataFrame(df_dic1,columns=['country','quality','category'])
print('df7:{}'.format(df7))
# 输出合并之后的数据集
print(pd.merge(df6,df7,left_on='category',right_on='category',how='left'))

#读取文件

pd.read_csv('df.csv',encoding='utf-8')
# 写入文件
#df.to_csv('df.csv',sep=',',header=True,index=True)

