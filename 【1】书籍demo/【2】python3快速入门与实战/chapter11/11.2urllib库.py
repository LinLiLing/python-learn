'''
urllib库
'''
import urllib.request
import urllib.error
import urllib.parse

## 11.2.1 urlopen函数

# 例11-1 以GET方式访问百度
'''
response = urllib.request.urlopen("http://www.baidu.com")
#response.read()获取网页内容，其数据是bytes字节类型，需要使用utf8编码对内容转码才能正常显示
print(response.read().decode("utf8"))
'''

# 例11-2 以POST方式访问“http://httpbin.org/post”测试网站
'''
#与HTTP请求一起发送的数据
param_dist = {"key":"hello"}
#调用urlencode函数将字典类型数据转成成字符串
param_str = urllib.parse.urlencode(param_dist)
#将传输的数据封装成一个bytes对象
param_datas = bytes(param_str,encoding="utf8")
#在urlopen函数中传入data参数值，表示将发送一个POST请求
response = urllib.request.urlopen("http://httpbin.org/post",data=param_datas)
#打印网站返回的响应内容
print(response.read())
'''

# 例11-3 设置请求超时时间
'''
response = urllib.request.urlopen("http://www.baidu.com",timeout=0.001)
print(response.read())
'''

# 例11-4 获取响应状态码
'''
response = urllib.request.urlopen("http://www.baidu.com")
#获取状态码
print(type(response))
print(response.status)
'''

# 例11-5 获取响应头信息
'''
#获取所有的响应头信息
print("headers:",response.getheaders())
#获取指定的头信息
print("header date:",response.getheader("Date"))
'''

## 11.2.2 urllib.request.Request类

#例11-6 伪装成浏览器发送POST请求
'''
url = "http://httpbin.org/post"
headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"}
data_dict = {"word":"hello world"}
data = bytes(urllib.parse.urlencode(data_dict),encoding="utf8")
request_obj = urllib.request.Request(url=url,data=data,headers=headers,method="POST")
# request_obj.add_header("User-Agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36")
response = urllib.request.urlopen(request_obj)
print(response.read().decode("utf8"))
'''

## 11.2.3 urllib.error异常处理

#例11-7 访问一个不存在的URL，捕获URLError异常
'''
url = "http://www.hhhdddmmm123.com"
try:
    request_obj = urllib.request.Request(url=url)
    response = urllib.request.urlopen(request_obj)
except urllib.error.URLError as e:
    print(e.reason)
'''

# 例11-8 访问一个不存在的URL，捕获HTTPError和URLError，并获取异常原因、响应状态码、请求头信息
'''
try:
    responese = urllib.request.urlopen("http://www.douban.com/abc")
except urllib.error.HTTPError as e:
    print("捕获HTTPError异常，异常原因：",e.reason)
    print("响应状态码：",e.code)
    print("请求头信息：",e.headers)
except urllib.error.URLError as err:
    print(err.reason)
'''