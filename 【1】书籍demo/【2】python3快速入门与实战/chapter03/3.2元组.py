'''
元组
'''
#例3-19 通过索引查询元组中元素

#定义一个元组，存储数据库相关连接信息，包括连接地址IP,端口号，用户名，密码
db_info = ("192.168.1.1",3306,"root","root123")
'''
ip = db_info[0]
port = db_info[1]
print("ip:{},port:{}".format(ip,port))
'''
#例3-20 使用for循环遍历元组
#for循环
for item in db_info:
    print(item)

#例3-21 使用while循环遍历元组
i = 0
while i< len(db_info):
    print(db_info[i])
    i += 1