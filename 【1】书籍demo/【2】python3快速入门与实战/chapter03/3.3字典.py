'''
字典
'''
#例3-22 使用字典存储个人信息,包含：姓名，年龄，性别，掌握的编程语言
user_info_dict = {"name":"小明","age":20,"gender":"male","program":"python"}
# print(user_info_dict)

#例3-23 查询小明掌握的编程语言
'''
program = user_info_dict["program"]
print("program:{}".format(program))
'''

#查询字典中不存在的键
#例3-24 查询小明的电话号码
'''
tel = user_info_dict["tel"]
print("tel:{}".format(tel))
'''

#get查询方法
#例3-25 使用get()方法查询小明的手机号
'''
tel = user_info_dict.get("tel")
print("tel:{}".format(tel))
'''
#例3-26 使用get()方法设置默认手机号，如果查询小明的手机号不存在则返回默认手机号
'''
tel = user_info_dict.get("tel","10086")
print("tel:{}".format(tel))
'''

#例3-27 向字典user_info_list中添加小明的手机号
'''
user_info_dict["tel"] = "13801234567"
# print(user_info_dict)
tel = user_info_dict.get("tel","10086")
print("原tel:{}".format(tel))
'''

#例3-28 小明更换了手机号，需要修改字典中小明的手机号
'''
user_info_dict["tel"] = "13866666666"
tel = user_info_dict.get("tel","10086")
print("新tel:{}".format(tel))
'''

#例3-29 为了保护隐私，小明要删除字典中自己的手机号
'''
print("删除手机号之前：",user_info_dict)
del user_info_dict["tel"]
print("删除手机号之后：",user_info_dict)
'''

#循环遍历列表
#例3-30 通过keys()方法获取字典内的所有键，循环遍历字典
'''
for key in user_info_dict.keys():
    print("{}:{}".format(key,user_info_dict[key]))
'''

#例3-31 通过values方法获取字典内的所有值，循环打印键值
'''
for value in user_info_dict.values():
    print(value)
'''
#例3-32 通过items()方法返回由字典内键值对组成的元素
#items返回键值对组成的元组列表
'''
for item in user_info_dict.items():
    print("---------------")
    print("item类型：",type(item))#打印item类型
    print("item:",item)
    print(item[0])#元组的第1个元素是key
    print(item[1])#元组的第2个元素是value
'''
#例3-33 使用两个变量接收items方法返回的由键和值组成的元组
for key,value in user_info_dict.items():
    print("{}:{}".format(key,value))