'''
列表
'''
#例3-1 存储班级内所有同学姓名
'''
name_list = ["小明","小白","小黑"]
print(type(name_list))#使用type函数查看变量类型
print(name_list)
'''
#例3-2 存储一个同学的详细信息
'''
#信息包含：姓名，年龄，身高，体重，是否会python
info_list = ["小明",20,180.5,80,True]
# print(info_list)
'''

#例3-3 查询存储同学信息列表中的姓名及该同学是否会python
'''
name = info_list[0] #列表中第1个元素，脚标是0
if_python = info_list[4]#列表中第5个元素，脚标是4
print("{}是否会Python:{}".format(name,if_python))
'''
#例3-4 查询列表索引越界错误
# if_python = info_list[5]#报索引越界错误

#例3-5 for循环遍历列表
'''
for i in range(0,len(info_list)):
    #通过索引查询列表元素值
    print(info_list[i])
'''
#例3-6 for循环遍历列表第二种方法
'''
for item in info_list:
    print(item)
'''

#例3-7 使用while循环遍历列表
'''
list_len = len(info_list)
i = 0
while i< list_len:
    print(info_list[i])
    i += 1
'''

#例3-8 嵌套列表
#存储多个同学的信息
info_lists = [["小明",20,180.5,80,True],["小白",18,175,70,True],["小黑",25,185,90,False]]
'''
#查询第一个同学的信息
print(info_lists[0])

#查询每个同学的姓名
print("所有同学姓名：")
#第1个脚标获取的是第一个同学的所有信息，第2个脚标获取的是一个同学的姓名
print(info_lists[0][0])
print(info_lists[1][0])
print(info_lists[2][0])
'''

#例3-9 使用append方法向info_list列表末尾添加新同学信息
'''
new_info = ["小壮",25,190,85,False]
info_lists.append(new_info)
print(info_lists)
'''

#例3-10 insert方法向列表指定位置添加元素
'''
new_info = ["小壮",25,190,85,False]
info_lists.insert(1,new_info)
print(info_lists)
'''

#例3-11 使用extend方法向info_lists列表中添加另外一个存储新同学信息的列表new_info_lists的所有新同学信息
'''
new_info_lists = [["小壮",25,190,85,False],["小牛",23,170,70,False]]
info_lists.extend(new_info_lists)
print(info_lists)
'''

#例3-12 使用+号拼接两个列表，组成新的列表
'''
name_list1 = ["小白","小黑","小明"]
name_list2 = ["小壮","小牛"]
name_list3 = name_list1 + name_list2
print("name_list1:",name_list1) # name_list1中元素不变
print("name_list2:",name_list2) # name_list2中元素不变
print("name_list3:",name_list3) # name_list1和 name_list2中元素的集合
'''

#例3-13 修改已定义的列表info_list中小明的体重值
'''
info_list = ["小明",20,180.5,80,True]
print("减肥之前体重：",info_list)
info_list[3] = 75
print("减肥之后体重：",info_list)
'''

#例3-14 使用del删除info_list中小明的年龄
'''
info_list = ["小明",20,180.5,80,True]
del info_list[1]
print(info_list)
'''
#例3-15 使用remove方法根据年龄值将其从列表中删除
'''
info_list = ["小明",20,180.5,80,True]
info_list.remove(20)
print(info_list)
'''

#例3-16 使用pop方法根据年龄索引将其从列表中删除
'''
info_list = ["小明",20,180.5,80,True]
info_list.pop(1)
print(info_list)
'''

#例3-17 列表切片的使用方法
'''
#列表name_core_list存储了全班考试成绩由高到低的前5名同学的名字
name_core_list = ["小明","小白","小黑","小壮","小牛"]
#取前三名同学的名字
top3 = name_core_list[:3]#从索引0开始，截取到索引2，切片是左闭右开，所以结束索引是3
print("top3:",top3)
#取第4名和第5名同学的名字
top4_5 = name_core_list[3:]#从索引3也就是第4个元素开始截取到列表最后
print("top4_5:",top4_5)
#取第1名、第3名、第5名同学的名字
top1_3_5 = name_core_list[0::2]#从头开始找，一直找到末尾，设置步长为2实现隔一个元素拿出来一个
print("top1_3_5:",top1_3_5)
'''

#例3-18 使用sort()方法对列表内的元素进行排序
'''
num_list = [6,2,8,13,97]
num_list.sort() #默认升序排列
print("升序：",num_list)
num_list.sort(reverse=True) #reverse=True降序排列
print("降序：",num_list)
'''

