'''
集合
'''

#例3-34 使用集合set存储学生学号
'''
student_id_set = {"20180101","20180102","20180103","20180104"}
#使用type函数查看student_id_set变量类型
print(type(student_id_set))
#使用len函数查询集合元素个数
print(len(student_id_set))
print(student_id_set)
'''
#例3-35 根据不同的序列使用set函数创建集合
#使用set(序列)函数创建集合
'''
#使用集合对列表元素去重,并且创建一个set集合
student_id_list = ["20180101","20180102","20180103","20180104","20180101","20180103"]
print("去重之前的列表：",student_id_list)
#使用set函数对student_id_list元素去重并创建新的集合
dist_studendt_id_set1 = set(student_id_list)
print("列表去重之后的集合：",dist_studendt_id_set1)

#使用集合对元组中的元素去重,并且创建一个set集合
student_id_tuple = ("20180101","20180102","20180103","20180104","20180101","20180103")
dist_studendt_id_set2 = set(student_id_tuple)
print("元组去重之后的集合：",dist_studendt_id_set2)

#注意下边的方式创建集合会把传入的字符串按照字符拆开，每个字符作为个体添加到集合中
string_set = set("hello")#把hello按字母拆分开，并且去重
print("字符串元素组成的集合：",string_set)
'''

#创建空集合
'''
none_set1 = set() #空集合，结果也是{}，只是跟空字典的形状相同
print(type(none_set1))
print(none_set1)
'''

#例3-36 判断一个学生的学号在存储学生学号的集合中是否存在
'''
student_id_set = {"20180101","20180102","20180103","20180104"}
student_id = "20180102"
if student_id in student_id_set:
    print("{}学号存在！".format(student_id))

student_id = "20180108"
if student_id not in student_id_set:
    print("{}学号不存在！".format(student_id))
'''

## 向集合中添加元素
student_id_set = {"20180101","20180102","20180103","20180104"}
#例3-37 使用add()方法将一个新同学的学号添加到存储学号的集合中
'''
student_id_set.add("20180105")
print(student_id_set)
'''

#例3-38 使用update方法将不同序列中存储的学号添加到存储学号的集合中
'''
#列表中的每一个元素作为整体添加进集合，同时对元素去重
student_id_set.update(["20180106","20180107","20180106"])
print("update列表：",student_id_set)

#元组中的每一个元素作为整体添加进集合，同时对元素去重
student_id_set.update(("20180108","20180109"))
print("update元组：",student_id_set)

#把一个集合并到另外一个集合
student_id_set_new = {"20180110","20180111"}
student_id_set.update(student_id_set_new)
print("update合并集合：",student_id_set)

#多个序列添加到集合中，使用逗号分隔，同时对所有序列中的元素去重
student_id_set.update(["20180112","20180113"],["20180113","20180114","20180115"])
print("update合并集合：",student_id_set)

# 如果向update中传入一个字符串，会把字符串按字符拆开，分别添加到集合中
student_id_set.update("0000000")
print("update字符串：",student_id_set)
'''

#例3-39 使用remove()方法删除存储学号集合中的"20180101"学号
'''
student_id_set = {"20180101","20180102","20180103","20180104"}
#删除学号20180101
student_id_set.remove("20180101")
print("执行remove方法后：",student_id_set)
#再次删除学号20180101将报错
student_id_set.remove("20180101")
'''
#例3-40 使用discard(元素)方法删除存储学号集合中的"20180101"学号
'''
student_id_set.discard("20180101")
print("执行discard方法后：",student_id_set)
student_id_set.discard("20180101")
print("再次执行discard方法后：",student_id_set)
'''
#例3-41 使用pop()随机删除一个学号
print("student_id_set:",student_id_set)
item = student_id_set.pop()
print("执行pop方法后：",student_id_set)
print("被删除的元素:",item)


## 集合常用操作
num_set1 = {1,2,4,7}
num_set2 = {2,5,8,9}

#例3-42 求两个数字集合的交集
'''
inter_set1 = num_set1 & num_set2
inter_set2 = num_set1.intersection(num_set2)
print(inter_set1)
print(inter_set2)
'''

#例3-43 求两个数字集合的并集
'''
union_set1 = num_set1 | num_set2
union_set2 = num_set1.union(num_set2)
print(union_set1) #{1, 2, 4, 5, 7, 8, 9}
print(union_set2)
'''

#例3-44 求两个数字集合的差集
'''
diff_set1 = num_set1 - num_set2
diff_set2 = num_set1.difference(num_set2)
print(diff_set1)
print(diff_set2)
'''