'''
datetime模块
'''

import datetime,time

#1.now()函数
#获取当前日期时间
'''
current_time = datetime.datetime.now()
print("默认格式：{}".format(current_time))

print("当前时间：",current_time)
print("year：",current_time.year)
print("month：",current_time.month)
print("day：",current_time.day)
print("hour：",current_time.hour)
print("minute：",current_time.minute)
print("second：",current_time.second)
'''
#计算时间差值
'''
start_time = datetime.datetime.now()
print("开始时间：",start_time)
#让程序睡眠2秒钟
time.sleep(2)
end_time = datetime.datetime.now()
print("结束时间：",end_time)
print("时间差：{}秒".format(end_time.second - start_time.second))
'''

#2.strftime(fmt)函数
#自定义日期时间格式化
'''
format_time = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
print("自定义格式：{}".format(format_time))
'''

#3.fromtimestamp(timestamp)函数
#将时间戳格式化为日期时间
'''
ts = time.time() #当前时间戳
print(datetime.datetime.fromtimestamp(ts)) #使用默认格式化方式
#自定义日期时间格式化
print(datetime.datetime.fromtimestamp(ts).strftime("%Y/%m/%d %H:%M:%S"))
'''

#4.timedelta类
#计算昨天的日期时间，方法：昨天的日期 = 今天的日期 - 1天的时间间隔
today = datetime.datetime.today()
print("今天的日期：{}".format(today.strftime("%Y-%m-%d")))
#1天的时间间隔
days = datetime.timedelta(days=1)#参数名称可以是days,hours,minutes,seconds等
#今天减去时间间隔得到昨天的日期时间
yesterday = today - days
print("昨天的日期：{}".format(yesterday.strftime("%Y-%m-%d")))