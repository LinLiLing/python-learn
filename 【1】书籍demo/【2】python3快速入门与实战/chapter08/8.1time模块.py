'''
time模块
'''

import time

#1．time()函数
#获取当前时间戳
# print(time.time())


#2.localtime([seconds])

'''
date_time = time.localtime()
print(date_time)
'''
'''
#获取当前时间戳
ts = time.time()
date_time = time.localtime(ts)
print(date_time)
#将指定时间戳转换成本地时间
print(time.localtime(1514829722))
'''

#3.strftime(fmt[,struct_time])
'''
#自定义日期时间格式化
#默认格式化当前时间
print(time.strftime("当前日期时间：%Y-%m-%d %H:%M:%S"))
#传入指定struct_time参数，对其格式化
print(time.strftime("指定日期时间：%Y-%m-%d %H:%M:%S",time.localtime(1514829722)))
'''

#4.strptime(string, format)
'''
#将格式化日期时间转换为struct_time元组
time_tuple = time.strptime("2018-09-11 09:01:38","%Y-%m-%d %H:%M:%S")
print(time_tuple)
'''

#5.mktime(p_tuple)
'''
#获取当前日期时间的时间戳
ts = time.mktime(time.localtime())
print(ts)
'''
#6.sleep(seconds)
#计时器
for t in range(3,-1,-1):
    print("倒计时：",t)
    if t != 0:
        time.sleep(1)
    else:
        print("go!")