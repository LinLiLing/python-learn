'''
变量作用于
局部变量与全局变量
'''

#例4-9 打印两个同学的个人信息
'''
def print_info1():
    #局部变量
    name = "小明"
    age = "20"
    print("name:{},age:{}".format(name,age))

def print_info2():
    #与print_info1函数中局部变量同名的局部变量
    name = "小黑"
    age = "25"
    print("name:{},age:{}".format(name,age))

print_info1()
print_info2()
'''

#例4-10 全局变量作为共享基准值在不同函数中参与计算
#基准值（全局变量）
basic_value = 1000
#求和
def sum(x):
    rs = basic_value + x
    print("{}与{}的和：{}".format(basic_value,x,rs))
#乘积
def product(x):
    rs = basic_value * x
    print("{}与{}的乘积：{}".format(basic_value,x,rs))
#函数调用
sum(20)
product(20)

#例4-11 修改全局变量值
#基准值（全局变量）
basic_value = 1000
#求和
def sum(x):
    #使用global关键字声明全局变量
    global basic_value
    #修改全局变量值
    basic_value = 1500
    rs = basic_value + x
    print("{}与{}的和：{}".format(basic_value,x,rs))
#乘积
def product(x):
    rs = basic_value * x
    print("{}与{}的乘积：{}".format(basic_value,x,rs))

#函数调用
print("basic_value:",basic_value)
sum(20)
print("basic_value:",basic_value)
product(20)
