'''
递归函数
'''
#例4-12 计算阶乘
def factorial_func(num):
    if num > 1:
        return num * factorial_func(num -1)
    else:
        # 结束递归的判断，当num=1时，已经递归到了最后一个值，直接返回1
        return num
#计算3的阶乘
print(factorial_func(3))