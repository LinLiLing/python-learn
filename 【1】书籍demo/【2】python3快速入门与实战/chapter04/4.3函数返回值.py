'''
函数返回值
'''
#例4-8 工资计算器

#计算社保缴费金额
def insurance(basic,**kvargs):
    #医疗保险缴费比例
    health = kvargs.get("health")
    #养老保险缴费比例
    pension = kvargs.get("pension")
    #社保缴费金额=医疗保险缴费金额（缴费基数*缴费比例）+养老保险缴费金额（缴费基数*缴费比例）
    cost = basic * health + basic * pension
    #返回社保缴费金额
    return cost

#计算缴纳工资个税金额
def tax(balance):
    #根据余额选择属于哪个缴税区间，按照对应的缴税比例缴税
    if balance <= 5000:
        #剩余工资小于等于5000不需要缴税
        return 0
    elif balance > 5000 and balance <= 10000:
        # 剩余工资大于5000并且小于等于10000税率为5%
        return balance * 0.05
    elif balance > 10000 and balance <= 30000:
        # 剩余工资大于10000并且小于等于30000税率为10%
        return balance * 0.1
    else:
        #剩余工资30000以上税率20%
        return balance * 0.2

#计算实发工资
def pay(basic):
    #社保中医疗保险和养老保险的缴费比例
    cost_dict = {"health":0.02,"pension":0.08}
    #计算社保缴费金额
    cost = insurance(basic,**cost_dict)
    #工资余额 = 缴费基数 - 社保花费
    balance = basic - cost
    #计算个税缴费金额
    tax_fee = tax(balance)
    #实发工资 = 基本工资 - 社保费用 - 个税
    pay_fee = basic - cost - tax_fee
    print("基本工资：{}，社保缴费：{}，个税：{}，实发工资：{}".format(basic,cost,tax_fee,pay_fee))

pay(8000)




