'''
匿名函数
'''
#例4-13 计算两个数字的和
sum = lambda x,y:x+y
#匿名函数调用
print(sum(10,20))

#例4-14 对两个数字根据不同的匿名函数求值
def x_y_compute(x,y,func): #func是匿名函数
    print("x={}".format(x))
    print("y={}".format(y))
    result = func(x,y) #对x和y两个参数使用func匿名函数进行计算
    print("result={}".format(result))

#传入的匿名函数用于求两个数的和
x_y_compute(3,5,lambda x,y:x+y)
#传入的匿名函数用于求两个数的乘积
x_y_compute(3,5,lambda x,y:x*y)