'''
函数参数
'''
#例4-2 定义带参函数，实现打印任何人信息的功能
'''
def print_user_info(name,age,gender):
    print("姓名:%s"%name)
    print("年龄:%d"%age)
    print("性别:%s"%gender)

name = "小黑"
age = 25
gender = "男"
print_user_info(name,age,gender)
'''

#缺省参数
#例4-3 计算两个数字的和
'''
def x_y_sum(x,y=20):
    rs = x + y
    print("{}+{}={}".format(x,y,rs))

#只传入一个参数x的值
x_y_sum(10)
print("----------")

#传入缺省参数y的实参30
x_y_sum(10,30)
'''

#命名参数
#例4-4 计算两个数字的和
'''
def x_y_sum2(x,y):
    rs = x + y
    print("{}+{}={}".format(x, y, rs))
#注意：使用命名参数的参数名称必须与函数定义时的形参名称相同
x_y_sum2(x=10,y=20)
#不按照函数定义的形参顺序，通过指定参数名称传入实参
x_y_sum2(y=30,x=15)
'''

#不定长参数
#例4-5 计算任意多个数字的和

def any_num_sum(*args):
    print("args参数值：",args)
    print("args参数类型：",type(args))
    rs = 0
    #判断元组是否不为空
    if len(args) > 0:
        #循环遍历元组，累加所有元素的值
        for arg in args:
            rs += arg
    print("总和：{}".format(rs))

# any_num_sum(20,30)
# any_num_sum(20,30,40,50)
# any_num_sum(20,30,40,50,60,70)

#例4-6 工资计算器
def pay(basic,**kvargs):
    print("kvargs参数值：",kvargs)
    print("kvargs参数类型：",type(kvargs))
    #扣除个税金额
    tax = kvargs.get("tax")
    #扣除社保费用
    social = kvargs.get("social")
    #实发工资 = 基本工资 - 缴纳个税金额 - 缴纳社保费用
    pay = basic - tax - social
    print("实发工资：{}".format(pay))
#函数调用
# pay(8000,tax=500,social=1500)


#例4-7 拆包方法使用
#求数字列表中所有元素的累加和
#数字列表
num_list = [10,20,30,40]
#由于any_num_sum函数设置了不定长参数*args，所以在传入列表时需要使用拆包方法"*列表"
any_num_sum(*num_list)
#打印一行分割线
print("----------------------------")
#个人工资计算器
#发工资之前需要扣除的费用
fee_dict = {"tax":500,"social":1500}
#由于pay函数设置了不定长参数**kvargs，所以在传入字典时需要使用拆包方法"**字典"
pay(8000,**fee_dict)
