'''
闭包
'''
#例4-15 求两个数字的和
'''
#普通函数，求两个数字的和
def sum(x,y):
    return x + y

#闭包，求两个数字的和
#外部函数
def sum_closure(x):
    #内部函数
    def sum_inner(y):
        #调用外部函数的变量x
        return x + y

    #外部函数返回值是内部函数的引用
    return sum_inner

rs1 = sum(10,1)
#打印rs1的值
print("rs1={}".format(rs1))
#打印rs1的类型
print("rs1的类型：{}".format(type(rs1)))

rs_func = sum_closure(10)
#打印rs_func的值
print("rs_func={}".format(rs_func))
#打印rs_func的类型
print("rs_func的类型：{}".format(type(rs_func)))
'''

#例4-16 闭包函数的使用
'''
rs2 = rs_func(1)
print("rs2={}".format(rs2))
print("rs2的类型：{}".format(type(rs2)))
'''

#例4-17 普通函数实现计数器

'''
参数说明：
    base：整型，表示基准值，计数器在基准值上累加，初始值0
    step：整型，表示步长，初始值1
'''
'''
def counter_func(base=0,step=1):
    return base + step

c1 = counter_func()
print("当前计数器值为：{}".format(c1))
c2 = counter_func(c1)
print("当前计数器值为：{}".format(c2))
c3 = counter_func(c2)
print("当前计数器值为：{}".format(c3))
'''

#例4-18 闭包实现计数器
def counter_closure(base=0):
    #在外部函数中定义一个列表类型的变量，存储计数器的累加基数
    cnt_base = [base]
    def counter(step=1):
        cnt_base[0] += step
        return cnt_base[0]
    return counter
counter = counter_closure()
print("当前计数器值为：{}".format(counter()))
print("当前计数器值为：{}".format(counter()))
print("当前计数器值为：{}".format(counter()))
