'''
贪婪与非贪婪模式
'''
import re

# 例10-23 贪婪与非贪婪模式对比
#贪婪模式
print("--------贪婪模式--------")
rs = re.findall("python\d*", "python2018") #任意多个数字
print(rs)
rs = re.findall("python\d+", "python2018") #至少出现一次数字
print(rs)
rs = re.findall("python\d{2,}", "python2018") #至少出现2次数字
print(rs)
re.findall("python\d{1,4}","python2018") #出现1到4次数字
print(rs)

#非贪婪模式
print("--------非贪婪模式--------")
rs = re.findall("python\d*?", "python2018")
print(rs)
rs = re.findall("python\d+?", "python2018")
print(rs)
rs = re.findall("python\d{2,}?", "python2018")
print(rs)
re.findall("python\d{1,4}?","python2018")
print(rs)