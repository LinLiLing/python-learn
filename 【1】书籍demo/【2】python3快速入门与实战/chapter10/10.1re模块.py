'''
正则表达式-re模块
'''

import re

## 10.1.1单字符匹配

# 例10-1 匹配一个字符串中是否包含“python”子串
'''
str = "python java python c++"
rs = re.match("python",str)
print(rs)
'''

# 例10-2 获取正则表达式匹配成功的子串
'''
str = "python java python c++"
rs = re.match("python",str)
print(rs.group())
'''

















