'''
单字符匹配
'''
import re

# 例10-3 使用“.”匹配除换行符“\n”之外的任意单个字符
'''
rs = re.match(".","1") #匹配一个包含数字的字符串
print(rs.group())
rs = re.match(".","a") #匹配一个包含单字符的字符串
print(rs.group())
rs = re.match(".","abc") #匹配一个包含多字符的字符串
print(rs.group())
'''

# 例10-4 使用多个点号匹配多个单字符
'''
rs = re.match("...","abc") #使用三个点号匹配三个单字符
print(rs.group())
rs = re.match(".","\n") #不会匹配\n，返回none
print(rs)
'''

# 例10-5 使用“\s”匹配任意空白字符

rs = re.match("\s","\t")#匹配tab键
print(rs)
rs = re.match("\s","\n") #匹配换行符
print(rs)
rs = re.match("\s"," ") #匹配空格
print(rs)

print("---------------")

# 例10-6 匹配“[ ]”中列举的字符，“[ ]”中列举的字符之间是或的关系

# 匹配以小h或者大H开头的字符串
rs = re.match("[Hh]", "hello")
print(rs)
rs = re.match("[Hh]", "Hello")
print(rs)
# 匹配0到9任意一个数字，方法一
rs = re.match("[0123456789]", "3")
print(rs)
# 匹配0到9任意一个数字，方法二
rs = re.match("[0-9]", "3")
print(rs)