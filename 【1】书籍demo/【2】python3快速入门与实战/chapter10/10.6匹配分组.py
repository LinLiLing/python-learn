'''
匹配分组
'''
import re

# 例10-13 使用分组匹配多种类型邮箱
'''
#匹配163、qq、outlook的邮箱
rs1 = re.match(r"\w{4,10}@(163|qq|outlook)\.com$","python2018@163.com")
print(rs1)
rs2 = re.match(r"\w{4,10}@(163|qq|outlook)\.com$","12345@qq.com")
print(rs2)
'''
# 例10-14 匹配网页标签
'''
#正常网页标签
html_data = "<head><titile>python</titile></head>"
rs = re.match(r"<.+><.+>.+</.+></.+>",html_data)
print(rs)

#错误的网页标签
html_data_err = "<head><titile>python</head></titile>"
rs = re.match(r"<.+><.+>.+</.+></.+>",html_data_err)
print(rs)
'''

# 例10-15 使用正则表达式分组匹配网页标签
'''
#\1表示使用前边第1组匹配到的规则，\2表示使用前边第2组匹配到的规则
#正确的网页标签
html_data = "<head><titile>python</titile></head>"
rs = re.match(r"<(.+)><(.+)>.+</\2></\1>",html_data)
print(rs)
#错误的网页标签
html_data_err = "<head><titile>python</head></titile>"
rs = re.match(r"<(.+)><(.+)>.+</\2></\1>",html_data_err)
print(rs)
'''

# 例10-16 正则表达式分组别名
#正确的网页标签
html_data = "<head><titile>python</titile></head>"
#?P<gname>不属于正则表达式的匹配规则，只是给分组起别名
rs = re.match(r"<(?P<g1>.+)><(?P<g2>.+)>.+</(?P=g2)></(?P=g1)>",html_data)
print(rs)
#错误的网页标签
html_data_err = "<head><titile>python</head></titile>"
rs = re.match(r"<(?P<g1>.+)><(?P<g2>.+)>.+</(?P=g2)></(?P=g1)>",html_data_err)
print(rs)
