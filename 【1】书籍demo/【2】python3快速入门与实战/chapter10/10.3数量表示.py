'''
数量表示
'''
import re

# 例10-7 检查用户信息是否完整
'''
#存储用户信息列表，每条用户信息包含三个字段：姓名，手机号，年龄
user_infos = ["Tom,13812345678,20","David,,30","Lilei,18851888888,25"]
for user in user_infos:
    # 使用正则检查用户信息是否完整
    rs = re.match("\w+,[0-9]{11},\d+",user)
    if rs != None:
        # 匹配成功打印用户信息
        print("用户信息：{}".format(rs.group()))
    else:
        print("用户信息不完整！")
'''

# 例10-8 验证手机号是否合法
def reg_phone(phone):
    #合法的手机号规则：由11位数字组成，第1位是1，第2位是3、5、7、8其中一个数字，第3到第11位是0-9的数字
    rs = re.match("1[3578]\d{9}", phone)
    if rs == None:
        return False
    else:
        print(rs.group())
        return True
#测试1：正确的手机号
print("----------测试1结果----------")
print(reg_phone("13612345678"))
#测试2：错误的手机号
print("----------测试2结果----------")
print(reg_phone("14612345678")) #第2位没有出现3、5、7、8其中一个
#测试3：正确的手机号+字符
print("----------测试3结果----------")
print(reg_phone("13612345678abc"))

