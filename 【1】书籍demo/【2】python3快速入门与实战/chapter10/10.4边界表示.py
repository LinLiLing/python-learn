'''
边界表示
'''
import re
# 10-9 结束边界在手机号验证中的应用
'''
def reg_phone(phone):
    #合法的手机号规则：由11位数字组成，第1位是1，第2位是3、5、7、8其中一个数字，第3到第11位是0-9的数字
    rs = re.match("1[3578]\d{9}$", phone)
    if rs == None:
        return False
    else:
        print(rs.group())
        return True
#测试1：正确的手机号
print("----------测试1结果----------")
print(reg_phone("13612345678"))
#测试2：错误的手机号
print("----------测试2结果----------")
print(reg_phone("14612345678")) #第2位没有出现3、5、7、8其中一个
#测试3：正确的手机号+字符
print("----------测试3结果----------")
print(reg_phone("13612345678abc"))
'''

# 例10-10 验证标识符是否合法
def reg_identifier(str):
    #正则表达式匹配以数字开头的字符串
    rs = re.match("^[0-9]\w*",str)
    if rs != None:
        #如果str以数字开头，则表示是非法标识符
        return False
    else:
        return True

print("标识符1_name是否合法：{}".format(reg_identifier("1_name")))
print("标识符name_1是否合法：{}".format(reg_identifier("name_1")))


# #非数字开头的字符串
# rs = re.match("[^0-9]\w+","ab2c")
# print(rs)
# rs = re.match("[^0-9]\w+","1ab2c")
# print(rs)
