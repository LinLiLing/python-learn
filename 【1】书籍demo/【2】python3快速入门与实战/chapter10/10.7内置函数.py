'''
内置函数
'''
import re

## 1.compile函数
'''
# 例10-17 复用正则表达式对象匹配邮箱地址
pattern = re.compile("\w{4,10}@163\.com$") #返回正则表达式对象
rs = re.match(pattern,"python2018@163.com")
print(rs)
rs = re.match(pattern,"vip_python@163.com")
print(rs)
rs = re.match(pattern,"abc@163.com")
print(rs)
'''

## 2.search函数
'''
# 例10-18 从字符串中查找是否包含“python”
rs = re.search("python","hi python,i am going to study python")
print(rs)
'''

## 3.findall函数
'''
# 例10-19 从用户信息中查找出所有手机号
infos = "Tom:13800000001,David:13800000002"
list = re.findall(r"1[3578]\d{9}",infos)
print(list)
'''
'''
# 例10-20 从用户信息中查找出所有邮箱地址
infos = "Tom:python_vip@163.com,David:12345@qq.com"
list = re.findall(r"(\w{3,20}@(163|qq)\.com)",infos)
print(list)
'''

## 4.finditer函数
'''
# 例10-21 从用户信息中查找出所有手机号
infos = "Tom:13800000001,David:13800000002"
iter_obj = re.finditer(r"1[3578]\d{9}",infos)
for iter in iter_obj:
    print(iter.group())
'''

## 5.sub函数
# 例10-22 将字符串中的所有空格替换成逗号
stu = "Tom 13800000001 Male"
stu_new = re.sub("\s",",",stu)
print("stu={}".format(stu))
print("stu_new={}".format(stu_new))