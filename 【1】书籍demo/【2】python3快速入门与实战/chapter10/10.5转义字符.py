'''
转义字符
'''
import re

## 转义字符
# 例10-11 使用正则表达式匹配163邮箱
'''
#合法的163邮箱地址以4到10个单词字符开始，以@163.com结束
#合法邮箱
rs = re.match("\w{4,10}@163.com$","python2018@163.com")
print(rs)
#非法邮箱
rs = re.match("\w{4,10}@163.com$","abc@163.com")
print(rs)
#非法邮箱
rs = re.match("\w{4,10}@163.com$","vip_python2018@163.com")
print(rs)
#将测试邮箱中的“.”用字母“h”代替
rs = re.match("\w{4,10}@163.com$","python2018@163hcom")
print(rs)

#使用转义字符"\"将"."转换为普通字符
rs = re.match("\w{4,10}@163\.com$","python2018@163hcom")#非法邮箱
print(rs)
rs = re.match("\w{4,10}@163\.com$","python2018@163.com")#合法邮箱
print(rs)
'''

## 原生字符串
'''
#小案例
str = "abc\\def"
print(str)

str = r"abc\\def"
print(str)
'''

# 例10-12 匹配字符串中的斜杠“\”
str = "\python"
#使用非原生字符串定义正则表达式匹配规则
rs = re.match("\\\\\w+",str)
print(rs)
#使用原生字符串定义正则表达式匹配规则
rs = re.match(r"\\\w+",str)
print(rs)
