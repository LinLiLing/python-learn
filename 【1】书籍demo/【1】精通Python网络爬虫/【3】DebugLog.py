import requests

# Get 请求
# url = 'https://www.baidu.com'
# data = requests.get(url=url,params={'user':'12345'}) #左边是变量名，右边是参数。data返回的是整体
# data.encoding = 'utf8'   #编码
# print(data.headers)
# print(data.status_code)

# Post 请求
url = 'https://www.baidu.com'
data = requests.get(url=url,data={'1':'2'}) #左边是变量名，右边是参数。data返回的是整体
data.encoding = 'utf8'   #编码
# print(data.headers)
print(data.status_code)