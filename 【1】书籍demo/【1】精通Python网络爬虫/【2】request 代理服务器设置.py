import requests
# 使用代理
# proxy:是一个字典的形式。http或者https这样的字段
proxy={ 
    'http':'http://120.196.186.248:9091'  #可以找找国内的一些免费ip
}
result = requests.get(url=r"http://httpbin.org/ip",proxies=proxy)
print(result.headers)


# Get 请求
# url = 'https://www.baidu.com'
# data = requests.get(url=url,params={'user':'12345'}) #左边是变量名，右边是参数。data返回的是整体
# data.encoding = 'utf8'   #编码
# print(data.headers)
# print(data.status_code)

# Post 请求
# url = 'https://www.baidu.com'
# data = requests.get(url=url,data={'1':'2'}) #左边是变量名，右边是参数。data返回的是整体
# data.encoding = 'utf8'   #编码
# print(data.status_code)