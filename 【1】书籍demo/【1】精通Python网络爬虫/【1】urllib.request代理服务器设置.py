# 定义一个自定函数：主要永爱实现代理服务器来爬取某个URL网页的功能
def use_proxy(proxy_addr,url):
    # 库的概念是具有相同功能模块的集合，也可以说是完成一定功能的代码集合。模块：包含并且有组织的代码片段为模块。包：是一个有层次的文件目录结构。
    import urllib.request  #导入urllib库下的request模块。
    proxy = urllib.request.ProxyHandler({'http':proxy_addr}) #设置对应的代理服务器信息,设置格式为：urllib.request.ProxyHandler({'http':代理服务器地址})
    opener = urllib.request.build_opener(proxy,urllib.request.HTTPHandler) #创建一个自定义的opener对象，第一个参数是代理信息，第二个参数为urllib.request.urlopen()打开对应网址爬取网页并读取，编码后赋值给变量data,最后返回data的值给函数。
    urllib.request.install_opener(opener) #创建全局默认的opener对象
    data = urllib.request.urlopen(url).read().decode('utf-8') #打开对应网址的网页并使用utf-8的方式进行读取
    return data
proxy_addr = '117.159.15.99:9091'
data = use_proxy(proxy_addr,"http://www.baidu.com")
print(len(data))