# 导入数据请求模块  ---> 第三方模块 需要 pip install requests  --> win +R 输入cmd
import requests
# 导入数据格式化模块 ---> 内置模块 不需要安装
import pprint
# 导入正则模块 ---> 内置模块 不需要安装
import re

home_url = 'https://www.kugou.com/yy/html/rank.html'
headers = {
    # user-agent 用户代理, 表示浏览器基本身份标识
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.0.0 Safari/537.36'
}
html_data_1 = requests.get(url=home_url, headers=headers).text
info_list = re.findall('a title="(.*?)".*?hidefocus="true" href="(.*?)" onclick', html_data_1)
# print("榜单集合",info_list[0])
for name, link in info_list[0:2]: #[11:] 韩版需要验证码
    name = re.sub('".*', '', name)
    # print(name, link)   #榜单地址
    # 发送请求获取数据
    # link = 'https://www.kugou.com/yy/rank/home/1-6666.html?from=rank'
    headers = {
        # user-agent 用户代理, 表示浏览器基本身份标识
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.0.0 Safari/537.36'
    }
    # 发送请求 response.text 获取文本数据
    html_data = requests.get(url=link, headers=headers).text
    """
    解析数据 ---> 根据返回数据类型, 以及你想要数据内容, 选择最适合的解析方法
        - 得到是字典数据, 使用字典取值 json解析
        - 得到文本数据 字符串 正则表达式
            re.findall() 通过re<正则模块>里面findall<找到所有>方法去提取数据
                从什么地方去找什么样数据内容
                从 html_data 去找 "Hash":"(.*?)", 这段内容 其中(.*?)就是我们想要的数据
                .*? 正则元字符, 表示可以匹配任意字符 除了换行符\n
    """
    Hash_list = re.findall('"Hash":"(.*?)",', html_data)
    album_id_list = re.findall('"album_id":(.*?),', html_data)
    # 我想要把列表一个一个提取出来, for循环
    for Hash, album_id in zip(Hash_list, album_id_list):
        # print(Hash, album_id)
        """
        1. 发送请求, 模拟浏览器对于url地址发送请求
            - 批量替换方法:
                ctrl + R 输入正则命令
                (.*?): (.*)
                '$1': '$2',
            - 报错: simplejson.errors.JSONDecodeError: Expecting value: line 1 column 1 (char 0)
                不是完整json数据格式, 所以没办法直接得到json字典数据
    
        采集榜单多个音频 ---> 分析请求参数变化规律
        """
        # 确定网址 当我们请求url地址太长的时候, 可以分段写 链接当中问号后面的内容属于请求参数
        url = 'https://wwwapi.kugou.com/yy/index.php'
        # 请求参数 --> 字典数据类型, 构建完整键值对形式 慢一点 1 快一点 2 请求参数不全可以不可以, 不一定
        data = {
            'r': 'play/getdata',
            # 来自于回调
            # 'callback': 'jQuery19107028558321633656_1658319044405',
            'hash': Hash,
            'dfid': '0jILs82Svsxu107QqV4dqfc9',
            'appid': '1014',
            'mid': '79ccf0ae0621ad3b03cebab6d7ef206d',
            'platid': '4',
            'album_id': album_id,
            '_': '1658319044410',
        }
        # 请求头伪装 --> 在开发者工具里面复制的
        headers = {
            # user-agent 用户代理, 表示浏览器基本身份标识
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.0.0 Safari/537.36'
        }
        # 发送请求 <Response [200]> 响应对象, 200状态码表示请求成功
        response = requests.get(url=url, params=data, headers=headers)
        # 获取数据 获取服务器返回响应数据 ---> 把请求参数里面callback删掉, 然后在获取response.json()
        # 格式化输出 更加方便字典取值  pprint.pprint(response.json())
        # 字典取值 ---> 键值对取值, 根据冒号左边的内容[键],提取冒号右边的内容[值]
        title = response.json()['data']['audio_name']
        audio_url = response.json()['data']['play_url']
        title = re.sub(r'[\/:*?"<>|]', '', title)
        if audio_url:
            print(title)
            print(audio_url)
            # 保存数据 ---> 需要发送请求 并且获取数据 response.content 获取二进制数据
            audio_content = requests.get(url=audio_url, headers=headers).content
            # with open('D:\\Ling_GF_Project\\python-learn\\【4】自己的小练习\\music\\' + title + '.mp3', mode='wb') as f:
            with open('music\\' + title + '.mp3', mode='wb') as f:
                f.write(audio_content)






