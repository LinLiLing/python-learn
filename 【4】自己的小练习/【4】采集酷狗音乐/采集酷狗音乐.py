# 1.发送请求，模拟浏览器对url地址发送请求
# 2.获取数据，获取服务器返回响应数据
# 3.解析数据，提取我们想要的数据
# 4.保存数据，保存音频到本地

# 导入数据请求模块
import requests
# 导入数据格式化模块
import pprint
# 导入正则模块
import re

'''
模块
'''
def cai_music(home_url,child_url):
    header = {
        'user-agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.0.0 Safari/537.36'
    }
    html_data_1 = requests.get(url=home_url,headers=header).text
    info_list = re.findall('a title="(.*)".*?hidefocus="true" href="(.*?)" onclick',html_data_1 )
    for name,link in info_list[0:2]:
        name = re.sub('".*','',name)
        html_data = requests.get(url=link,headers=header).text
        Hash_list = re.findall('"Hash":"(.*?)",',html_data)
        album_id_list = re.findall('"album_id":(.*?),', html_data)
        
        for Hash, album_id in zip(Hash_list, album_id_list):
            data = {
                'r': 'play/getdata',
                # 来自于回调
                # 'callback': 'jQuery19107028558321633656_1658319044405',
                'hash': Hash,
                'dfid': '0jILs82Svsxu107QqV4dqfc9',
                'appid': '1014',
                'mid': '79ccf0ae0621ad3b03cebab6d7ef206d',
                'platid': '4',
                'album_id': album_id,
                '_': '1658319044410',
            }
            headers = { 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.0.0 Safari/537.36'}
            response = requests.get(url=child_url, params=data, headers=headers)
            title = response.json()['data']['audio_name']
            audio_url = response.json()['data']['play_url']
            title = re.sub(r'[\/:*?"<>|]','',title)
            if audio_url:
                print(title)
                print(audio_url)
                audio_content = requests.get(url=audio_url, headers=header).content
                # with open('D:\\Ling_GF_Project\\python-learn\\【4】自己的小练习\\music\\' + title + '.mp3', mode='wb') as f:
                #     f.write(audio_content)
                return '爬取完成'
# print(cai_music.__annotations__) #查看函数注释

cai_music('https://www.kugou.com/yy/html/rank.html','https://wwwapi.kugou.com/yy/index.php')