# -*- coding:utf8 -*-
# 开发者：Ling
# 微信号：LLL1126803354
# 更新时间：20220802
# 开发语言：python
# 代码描述：爬取国家统计局2021年行政区划数据整理为csv文件


# 模块导入
import requests
from bs4 import BeautifulSoup
import re
import csv
import time
import os

from sqlalchemy import false, true
# from package_runoob.DYcsvfile  import  DyCsv


# 全局变量
BASEURL = 'http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2021/'
# 表头信息
COLUMTITLE = ['省（直辖市，自治区，特别行政区）', '地级市（州，旗）','县（区，县级市）', '乡（镇，街道）', '村']


# http下载器
def get_html(url):
    html = requests.get(url=url)
    html.encoding = 'utf8'
    document = BeautifulSoup(html.text, 'lxml')
    return document


# html解析器(省（直辖市，自治区，特别行政区）),返回全部省份名称和下级链接
def parse_html(document):
    soup = document.select('table.provincetable tr.provincetr a')
    data = [(re.sub(r'\.html.*$', "", i.get('href')),
             BASEURL + i.get('href'), i.text) for i in soup]
    return data


# html解析器(地级市（州，旗）)，返回全部市名称和下级链接-----document:'解析后的html代码'，provinceId:'省份地址序号',provinced:省份
def parse_html2(document, provinceId, provinced):
    soup = document.select('table.citytable tr.citytr')
    data = [
        (provinceId, BASEURL + i.select('a')
         [1].get('href'), provinced, i.select('a')[1].text)
        for i in soup
    ]
    return data


# html解析器(县（区，县级市）)，返回全部区名称和下级链接-----document:'解析后的html代码'，provinceId:'省份地址序号',provinced:省份，city:地级市
def parse_html3(document, provinceId, provinced, city):
    soup = document.select('table.countytable tr.countytr')
    data = []
    for i in soup:
        try:
            county = re.sub(r'\/.*$', '', i.select('a')[1].get('href'))
            data.append((provinceId + '/' + county, BASEURL + provinceId + '/' +
                         i.select('a')[1].get('href'), provinced, city, i.select('a')[1].text))
        except:
            continue
    return data


# html解析器(乡（镇，街道）)，返回全部名称和下级链接-----document:'解析后的html代码'，provinceId:'省份地址序号',provinced:省份，city:地级市，county：县区
def parse_html4(document, provinceId, provinced, city, county):
    soup = document.select('table.towntable tr.towntr')
    data = []
    for i in soup:
        try:
            data.append((provinceId, BASEURL + provinceId + '/' + i.select('a')
                         [1].get('href'), provinced, city, county, i.select('a')[1].text))
        except:
            continue
    return data


# tml解析器(村)-----document:'解析后的html代码'，provinceId:'省份地址序号',provinced:省份，city:地级市，county：县区，town：乡镇街道
def parse_html5(document, provinced, city, county, town):
    soup = document.select('table.villagetable tr.villagetr')
    data = []
    for i in soup:
        try:
            data.append([provinced, city, county, town, re.sub(
                '(居.*$)|(民.*$)|(委.*$)', '', i.select('td')[2].text)])
        except:
            continue
    return data


# 进度条
def progress_bar(str,num):
    j = "#"; k = "="; t = "|/-\\"; 
    for i in range(0, num + 1):
        j += "#"; k += "="; s = ("=" * i) + (" " * (num - i))
        print("%s[%s][%s][%.2f" % (str,t[i%4],s,(i/num*100)), "%]",end='\r')
        time.sleep(0.1)


# 调用获取省市区县街道地址，写入csv文件中
def get_area():
    html = get_html(url=BASEURL)
    # 获取省名+市区的链接
    data = parse_html(html)
    # len: 31, 共32个，分8个每组，先爬取前8个。 7,15,23,31。  j:序号，i:值
    for j, i in enumerate(data):
        # if  1 < j < 10 :  # 爬取一个试试
            print("序号", j)
            isUrl = write_txt(i[1])
            if isUrl == false:
                html = get_html(url=i[1])
                data = parse_html2(document=html, provinceId=i[0], provinced=i[2])

                for j in data:
                    isUrl = write_txt(j[1])
                    if isUrl == false:
                        html = get_html(url=j[1])
                        data = parse_html3(
                            document=html, provinceId=j[0], provinced=j[2], city=j[3])

                        for z in data:
                            isUrl = write_txt(z[1])
                            if isUrl == false:
                                html = get_html(url=z[1])
                                data = parse_html4(
                                    document=html, provinceId=z[0], provinced=z[2], city=z[3], county=z[4])
                            
                                for x in data:
                                    isUrl = write_txt(x[1])
                                    if isUrl == false:
                                        html = get_html(url=x[1])
                                        data = parse_html5(
                                            document=html, provinced=x[2], city=x[3], county=x[4], town=x[5])
                                        progress_bar(x[2]+x[3]+x[4]+x[5],32)
                                        write_cvs(data,"./address.csv")
        # else:
        #     break
    return '执行完毕！'


# 将数据写入csv操作
def write_cvs(data,file):
    '''
        data : 保存的数据
        file : 文件的地址
    '''
    # 文件存在
    if os.path.exists(file):
        sz = os.path.getsize(file)
        # 判断文件内容是否为空
        if not sz:
            data.insert(0,COLUMTITLE)
            # 'w'重新写入
            with open(file, 'w', encoding='utf-8_sig', newline='') as f: 
                writer = csv.writer(f)
                writer.writerows(data)
                print('完成数据写入CSV文件！')
        else:
            # 'a'追加写入
            with open(file, 'a', encoding='utf-8_sig', newline='') as f: 
                # f.write("\n")
                writer = csv.writer(f)
                writer.writerows(data)
                print('完成追加数据写入写入CSV文件！')
    # 文件不存在
    else:
        with open(file, 'a', encoding='utf-8_sig', newline='') as f:
            writer = csv.writer(f)
            writer.writerows(data)
            print('新建csv文件，并将数据写入CSV文件！')


# 保存地址(如果地址存在则不进行爬取操作)
def write_txt(url):
    file = "./addressUrl.txt"
    # 变量isUrl 为 false 时候就是没有没有存在
    isUrl = false
    if url != '':
        with open(file, 'r', encoding='utf-8') as f:
            for line in f.readlines():
                if url.strip() == line.strip() : 
                    isUrl = true
        if isUrl == false :
            with open(file, 'a+', encoding='utf-8') as f:
                f.write(url+"\n")    
        return isUrl

    

# write_txt("http://www.baidu.com")
# write_txt("http://www.baidddddu.com")
# 执行
get_area()





