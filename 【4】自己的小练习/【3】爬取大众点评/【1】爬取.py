import requests
import  json
import  pandas as pd
import font


headers = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "Accept-Language": "zh-CN,zh;q=0.9",
    "Cache-Control": "max-age=0",
    "Connection": "keep-alive",
    "Upgrade-Insecure-Requests": "1",
    'Cookie':'fspop=test; cy=1; cye=shanghai; _lxsdk_cuid=18006e84946c8-0edfb5359bc9cf-1a343370-144000-18006e8494683; _lxsdk=18006e84946c8-0edfb5359bc9cf-1a343370-144000-18006e8494683; _hc.v=26a2d697-b910-5ca3-2811-7cb9de72b061.1649383329; s_ViewType=10; dplet=54a75cdaf24b303d3f910da18f7722de; dper=4a8592a6f6069c93adacd0d4efa1208e88ad3f679ac3884773372a379298967e9afc526b24332076b0f0409a6044bd2f677d3ff6943921a4f97c10ac407f85f59c4531048bb3ff331e87b08beac013239986b3821d73a2d7ad662074a343d534; ua=%E5%B0%8F%E7%99%BD%E8%8F%9C; ctu=43203cd8c3de60575d04dea3547e7bc86b19a461a533987b78fafea2df8f4bd5; ll=7fd06e815b796be3df069dec7836c3df; Hm_lvt_602b80cf8079ae6591966cc70a3940e7=1649383329,1649421529,1649469353; _lx_utm=utm_source%3DBaidu%26utm_medium%3Dorganic; Hm_lpvt_602b80cf8079ae6591966cc70a3940e7=1649469364; _lxsdk_s=1800c08ea8e-63d-fe7-70e%7C%7C57',
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.75 Safari/537.36"
}
url = "http://www.dianping.com/ajax/json/shopDynamic/allReview?shopId=Emnne5KsrYwPeJ4I&cityId=1&shopType=10&tcv=0x0xrqr5le&_token=eJxVj19vgjAUxb9LnxtooRQh8cG%2FiYBzo1Y0iw8MmSIWHe1EXPbdVzL3sOQm59zfvSe59wvUsx3wMUKIYAiueQ18gA1kUACBknpCiUeoRyxquy4E2X%2FmYs3e6tUY%2BK%2FYsSl0bbTtSKzBL%2BlRsoUPa1tbaBFd3c5Mr4CDUhffNJumMXZFWl2Kam9kZ2HKw%2FliTkRV5U4o603znAdkpk8COimWXdKhCLrI7UDZAa3pQ9VfP9fP6JAs9pV2eXBbMknkx3s8l8sVb1svZMxqowxHjNvRfaKeOLsu2lFvwOqjcD6Tk6jO6ylKkiGNqVgXoxuff4xwoMYvNi9Kdc9uiyi8IpbhRbwLNuXJK9PV8MCnQ56IPC3D%2FWRgDuQRN%2F0%2B%2BP4BbKFpyA%3D%3D&uuid=26a2d697-b910-5ca3-2811-7cb9de72b061.1649383329&platform=1&partner=150&optimusCode=10&originUrl=http%3A%2F%2Fwww.dianping.com%2Fshop%2FEmnne5KsrYwPeJ4I"
response = requests.get(url, headers=headers).text
data_json=json.loads(response)

all_user=[]
all_comment=[]
for item in data_json['reviewAllDOList']:
     user=item['user']['userNickName']
     comment=item['reviewDataVO']['reviewData']['reviewBody']
     all_user.append(user)
     all_comment.append(comment)
     
#print(all_comment)
all_comData=[]
# 处理所有的评论
for  strr in all_comment:
    # 把获取到的字符串，去掉 标签
    string=strr.replace('<svgmtsi class="review">','')
    string2=string.replace(';</svgmtsi>','')
    ingergrate_str=string2.split('&')

    intergrate=[]
    # 判断有没有 加密的字符串,有就替换，没有就，正常输出
    for item in ingergrate_str:
        try:
            k=item[0]  # 截取第一个字符，判断是不是这种 【#xe2da聚】 的格式
            if k in ['#']:
                # 拼凑关键字,在下载的css文件里面 查找
                key='uni'+item[2:6]
                integrate_str=font.woff_dict(key)+item[6::]  # 只替换 【#xe2da】  剩下的 【聚】  保留 》》结果展示：【的聚】
                intergrate.append(integrate_str)
                #print(item)
            else:
                intergrate.append(item)
            # 把单个评论的列表数据,整合成一条
            data=''.join(intergrate)
        except : continue
    # 把for 循环的数据都 放在一个列表里面
    all_comData.append(data)

#输出至excel中
data=pd.DataFrame({'用户名':all_user,'评论':all_comData})
data.index+=1
data.to_excel(r'E:\pythonProject\spieder项目\da_comment\shuju\评论.xlsx')
print(len(all_comData))
print('完成')
