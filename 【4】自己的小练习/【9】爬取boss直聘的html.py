from email.headerregistry import Address
import requests
from bs4 import BeautifulSoup
import csv

# 岗位列表变量
ALLJOB = [['公司名','联系人','地址','薪资待遇','经验限制','福利','岗位技能']]

# 请求Boss直聘的岗位查询接口
def get_html(page):
    # 异步请求的地址,查询条件（前端开发、天河区、白云区、越秀区。薪资待遇10-20k、工作经验不限）
    url =  'https://www.zhipin.com/web/geek/job?query=%E5%89%8D%E7%AB%AF%E5%BC%80%E5%8F%91&city=101280100&experience=104,101&degree=202&salary=405&areaBusiness=440104,440111,440106'
    headers = {
        'Host': 'www.zhipin.com',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Accept-Encoding': 'gzip, deflate, br',
        'Referer': 'https://www.zhipin.com/web/geek/job?query=%E5%89%8D%E7%AB%AF%E5%BC%80%E5%8F%91&city=101280100&experience=104,101&degree=202&salary=405&areaBusiness=440104,440111,440106',
        'Cookie': 'lastCity=101280100; sid=sem_pz_bdpc_dasou_title; __zp_seo_uuid__=fad55e50-ed56-4a7e-9c4a-643321dd869a; __g=sem_pz_bdpc_dasou_title; Hm_lvt_194df3105ad7148dcf2b98a91b5e727a=1661581517; wd_guid=113c25a6-dc54-44bd-b9a8-f2a2826b3027; historyState=state; _bl_uid=j0ln17z5b1eiaLq0noXI4aj7y544; _9755xjdesxxd_=32; YD00951578218230%3AWM_NI=KEtSGXuFdBr%2FAp79%2Bn1w5bAT%2Bx1%2BTA0whzXRx6%2BH786S7ZPYiVIP3PpmOQrXPzP7avu8pVptfVoB8AfWQ2IV9IQ2QeBQ5tn%2F5Asf78GQqfiYY5KlTpGVpUI1XWyFlP5qT2o%3D; YD00951578218230%3AWM_NIKE=9ca17ae2e6ffcda170e2e6eea6cc4b83adff9ad76e87b48fa7c14e838a9f83c15da6bbfcb7b339f893afaddb2af0fea7c3b92a899bbcb4c654b79aaca3f12583b0f9daaa66f3a9fb83c460e9a8ffccd74bf4a9a89bdb5abceae5a9b634f68df9bbd353ab8a87b1b25cb4b381b2f15494a6a0d8e47eb7e8c0b7f380a68aa1a3bb4ba6899fd3cc5efb979fd3d46f828ffb95ed25f79fbd87b634858fbf87ec539beb858dd77c858ac0b6cc3b9c89b7a7ef5cf7af9cd3ee37e2a3; YD00951578218230%3AWM_TID=ZXsJoEkTsw9FRRRUEELUDeapVky4JPLG; Hm_lpvt_194df3105ad7148dcf2b98a91b5e727a=1661587202; __zp_stoken__=91d9eSBRbLUU6CDxOSEV6E0EERHRldUUJZzp5VHpRFC1BbXolfFpfZnE%2BAUp4Jw0GE3ctNWABanRVfhAiF05JPg0cbGIDXFFaSRN4HEE5eBQIcH9iJR8mSCgcL0UtHGU1H3ggOghxMCw7JSd%2BVU1fA3gfG0tMKhhjDA9sXWN7Yj1ZBQ1DDzJZMFpaR01vJVc8RAZQWDhNNA%3D%3D; gdxidpyhxdE=6Q%5C6T3UNhTCAERGa2M3SQGcxb4hGjoRzMNMVDvmb7%2Bh4tc7fOBbNKJkWs2kByXUzWpalDbIUHPqQigNuVjw%2BQXgGwKp0UuS%5C0X0R2wOwyRAuio0M18E%5C5jG0RNWj%5ChRu5PhK6mWDWo%5COUNcAH3Y9lEs2ChoLE2m3RM16j8O2w4j%2BpuqX%3A1661591229216; __c=1661581517; __l=r=https%3A%2F%2Fwww.baidu.com%2Fother.php%3Fsc.a00000jwxXBa_ucFgchLgu8J4Ix3_73_eUB7LK9b8JWmBJzHoxV2SxyC8h9DsXJYGD5UsJCug18QyJI1Vob9Gzftf_9KR8fpbP6mBKSjZBBr3IZmiXSZ3CWHOPuGyxTbbLKhTL-MM3oU29Yf3RkC9X693BZDyE8tErNchVf32oc6SiYckFDz1OghDYllKVigU-12HDKab_Q3qiDB346iouD7MjGj.7D_NR2Ar5Od663rj6t8AGSPticrtXFBPrM-kt5QxIW94UhmLmry6S9wiGyAp7BEIu80.TLFWgv-b5HDkrfK1ThPGujYknHb0THY0IAYqmhq1Tqpkko60IgP-T-qYXgK-5H00mywxIZ-suHY10ZIEThfqmhq1Tqpkko60ThPv5H00IgF_gv-b5HDdn1m3rjbLPj00UgNxpyfqnHfvnW0znHn0UNqGujYknHmzn1ndnsKVIZK_gv-b5HDzrjcv0ZKvgv-b5H00pywW5R9rf6KspyfqP0KWpyfqrjf0mLFW5HmYrHc4%26dt%3D1661581512%26wd%3Dboss%25E7%259B%25B4%25E8%2581%2598%26tpl%3Dtpl_12826_27888_0%26l%3D1536889740%26us%3DlinkVersion%253D1%2526compPath%253D10036.0-10032.0%2526label%253D%2525E4%2525B8%2525BB%2525E6%2525A0%252587%2525E9%2525A2%252598%2526linkType%253D%2526linkText%253D&l=%2Fwww.zhipin.com%2Fweb%2Fgeek%2Fjob%3Fquery%3D%25E5%2589%258D%25E7%25AB%25AF%25E5%25BC%2580%25E5%258F%2591%26city%3D101280100%26experience%3D104%2C101%26degree%3D202%26salary%3D405%26areaBusiness%3D440104%2C440111%2C440106&s=3&g=%2Fwww.zhipin.com%2Fguangzhou%2F%3Fsid%3Dsem_pz_bdpc_dasou_title&friend_source=0&s=3&friend_source=0; __a=25806131.1661581517..1661581517.13.1.13.13',
    }
    response = requests.get(url=url,headers=headers)
    response.encoding = 'utf8'
    jobList = response.json()['zpData']['jobList'] # 获取岗位的数据列表
    for i in jobList:
        brandName = jobList[i].brandName
        bossName = jobList[i].bossName
        address = jobList[i].cityName +jobList[i].areaDistrict + jobList[i].businessDistrict
        salaryDesc = jobList[i].salaryDesc #薪资
        jobLabels = jobList[i].jobLabels #经验限制
        welfareList = jobList[i].welfareList #福利
        skills = jobList[i].skills # 岗位技能
        ALLJOB.append([brandName,bossName,address,salaryDesc,jobLabels,welfareList,skills])
    print(ALLJOB)
    document = BeautifulSoup(response.text,'lxml')
    return document


# 调用
get_html(1)
