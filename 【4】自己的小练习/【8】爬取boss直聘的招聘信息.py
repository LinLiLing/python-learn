# -*- coding:utf-8 -*-
import requests
from bs4 import BeautifulSoup
import csv

# 岗位列表变量
ALLJOB = [['公司名','联系人','城市','区域','位置','薪资待遇','经验限制','福利','岗位技能']]

# 请求Boss直聘的岗位查询接口
def get_html(page):
    # 异步请求的地址,查询条件（前端开发、天河区、白云区、越秀区。薪资待遇10-20k、工作经验不限）
    url =  'https://www.zhipin.com/wapi/zpgeek/search/joblist.json?scene=1&query=%E5%89%8D%E7%AB%AF%E5%BC%80%E5%8F%91&city=101280100&experience=104,101&degree=202&industry=&scale=&stage=&position=&salary=405&multiBusinessDistrict=440104,440111,440106&page='+str(page)+'&pageSize=30'
    headers = {
        'Host': 'www.zhipin.com',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Accept-Encoding': 'gzip, deflate, br',
        'Referer': 'https://www.zhipin.com/web/geek/job?query=%E5%89%8D%E7%AB%AF%E5%BC%80%E5%8F%91&city=101280100&experience=104,101&degree=202&salary=405&areaBusiness=440104,440111,440106',
       
    }
    #  'Cookie': 'lastCity=101280100; wd_guid=113c25a6-dc54-44bd-b9a8-f2a2826b3027; historyState=state; _bl_uid=j0ln17z5b1eiaLq0noXI4aj7y544; _9755xjdesxxd_=32; YD00951578218230%3AWM_TID=ZXsJoEkTsw9FRRRUEELUDeapVky4JPLG; __g=-; Hm_lvt_194df3105ad7148dcf2b98a91b5e727a=1661581517,1661668433; Hm_lpvt_194df3105ad7148dcf2b98a91b5e727a=1661668433; YD00951578218230%3AWM_NI=Wo0sF1qnly9unr1cFcIwrQb9CFHj0XzinSITOMTjvhT5wW2dvw2X5cPGxUJblxzdKmv%2B%2BH8lp7U9KzjKNyaulJncHkHnlE5tcFwy%2FU%2BYuqA3PWBzmMRP%2FPeeKFdpiEXhN2Y%3D; YD00951578218230%3AWM_NIKE=9ca17ae2e6ffcda170e2e6eea3c46b919b9ab5f279ede78eb6d15b878a9a82c44da7b985a9cc4fbae786b1fb2af0fea7c3b92a95b10094c53bb3b2a49abc7faeaeff84c5698ba9beb2e133819a888bb354bab7a2b4cc608d939c8bf56498ebb9b8b170b6a9feb9b8699499b8b6c96ab0a8b685ca67b6f5e5a9bb68b4e99bdaca72aa95829ae47fb096a284cb618cb2a4a6b880a19ab99aae5fe987bb92c43994898e9bf0479290e1b1e86f899aac91ec4f9cb882d3dc37e2a3; __zp_seo_uuid__=6c5899ba-e9e9-4dd6-98da-4cc7ff31a563; warlockjssdkcross=%7B%22distinct_id%22%3A%22182e33fbf4625c-067f1efb90ee01-26021d51-753228-182e33fbf47a66%22%2C%22first_id%22%3A%22%22%2C%22props%22%3A%7B%7D%2C%22device_id%22%3A%22182e33fbf4625c-067f1efb90ee01-26021d51-753228-182e33fbf47a66%22%7D; wljssdk_cross_new_user=1; gdxidpyhxdE=qJ3WjYDoS%2FcETzegdk7oKkaCgVy9%2FSb1KoJzYRZX7gBZz1sMaNRKl3w3BTrX3NdGTbAu1ug%2BotwAI3kRDCwLWuUzdjmEcEmKrpn6EvVhjQ21ZKiV10%5CTtV6IkDYtPoB9MpJNfqKwOOlaYASKmPAfmCRKo2xdR8t9xvGZm%5C3191vaGOGD%3A1661671875791; __zp_stoken__=395deC3tubGUDLFMDHwUnC3ZFOgg1KXEzFj9vXgQDEjcXeS4EMCxoSWtxWnoFFX8qWkB6CnBUaEdgIGl8GSN4UFpMRgQabglNIBIlGDsEDGcSe3JaNWlFFVxkPgsdcWMoMjxMNToJc29yVEFiX2I4cilJBHFYVztEextPby00fA%3D%3D; __c=1661668431; __l=r=https%3A%2F%2Fbaike.zhipin.com%2F&l=%2Fwww.zhipin.com%2Fweb%2Fgeek%2Fjob%3Fquery%3D%25E5%2589%258D%25E7%25AB%25AF%25E5%25BC%2580%25E5%258F%2591%26city%3D101280100%26experience%3D104%2C101%26degree%3D202%26salary%3D405%26areaBusiness%3D440104%2C440111%2C440106&s=3&friend_source=0&s=3&friend_source=0; __a=25806131.1661581517.1661581517.1661668431.33.2.17.33'
    session = requests.session()
    response = session.get(url=url,headers=headers)
    response.encoding = 'utf8'
    try:
        jList = response.json()['zpData']['jobList'] # 获取岗位的数据列表
        for i in jList:
            brandName = str(i['brandName']) #公司名
            bossName = str(i['bossName']) #联系人
            cityName =  str(i['cityName']) #城市 
            areaDistrict = str(i['areaDistrict']) #区域 
            businessDistrict = str(i['businessDistrict']) #位置
            salaryDesc = str(i['salaryDesc']) #薪资
            jobLabels = str(i['jobLabels']) #经验限制
            welfareList = str(i['welfareList']) #福利
            skills = str(i['skills']) # 岗位技能
            ALLJOB.append([brandName,bossName,cityName,areaDistrict,businessDistrict,salaryDesc,jobLabels,welfareList,skills])
        print(ALLJOB)
        write_csv(ALLJOB)
    except:
        print("出错了，可能Cookie已经失效！请重新获取Cookie~")


# alist 测试数据
# alist = [['公司名', '联系人', '地址', '薪资待遇', '经验限制', '福利', '岗位技能'], ['蓝联科技', '肖女士', '广州', '7-12K', "['1-3年', '大专']", "['五险一金', '带薪年假', '年终奖', '交通补助', '员工旅游', '加班补助', '节日福利', '零食下午茶']", "['前端开发', 'HTML', 'CSS', 'JavaScript', 'CSS3', 'HTML5', 'ES6', 'ES5']"], ['广州百云互联科技', '林女士', '广州天河区黄村', '8-12K', "['1-3年', '大专']", "['节日福利', '零食下午茶', '带薪年假', '加班补助', '年终奖']", "['HTML', 'CSS', 'HTML5', '前端开发', 'Vue', 'Vue.js']"], ['品推科技', '李女士', '广州白云区金沙洲', '12-24K', "['1-3年', '大专']", "['员工旅游', '零食下午茶', '五险一金', '交通补助', '通讯补贴', '团建聚餐', '节日福利', '带薪年假', '生日福利']", "['HTML', 'CSS', 'MVC开发', '前端开发', 'PC端', '移动端', 'Vue']"], ['易娱网络', '梁女士', '广州天河区龙口', '15-20K·13薪', "['1-3年', '大专']", "['五险一金', '零食下午茶', '员工旅游', '定期体检', '交通补助', '带薪年假', '全勤奖', '年终奖', '餐补', '补充医疗保险', '节日福利']", "['HTML', 'Android', 'iOS', 'C#']"], ['王牌娱乐', '王女士', '广州天河区车陂', '8-13K', "['1-3年', '大专']", "['定期体检', '工龄奖', '住房补贴', '年终奖', '交通补助', '加班补助', '零食下午茶', '节日福利', '餐补', '员工旅游', '股票期权', '全勤奖']", "['HTML', 'React', '移动端', '小程序']"], ['赛脑', '林女士', '广州越秀区五羊新城', '10-15K·13薪', "['1-3年', '大专']", "['五险一金', '全勤奖', '年 终奖', '带薪年假', '员工旅游', '交通补助', '节日福利', '零食下午茶']", "['网络协议', 'googleplay', '跨境电商', 'google pla', '工具开发', '海外媒体', '海外工具应用', 'Android']"], ['小棒棒科技', '陈先生', '广州天河区东圃', '8-13K', "['1-3年', '大专']", '[]', "['前端开发', 'JavaScript', 'React', 'JQuery', 'Vue', '小程序', 'PC端', '移动端']"], ['伴多久', '钟女士', '广州天河区五山', '9-13K', "['1-3年', '大专']", "['节日福利', '零 食下午茶', '带薪年假']", "['前端开发', 'WEB开发', 'HTML', 'CSS', 'JavaScript', 'CSS3', 'HTML5', 'Web端']"], ['新谷公安大数据', '禹先生', '广州越秀区五羊新城', '9-14K', "['1-3年', '大专']", "['五险一金', '年终奖', '带薪年假', '补充医疗保险', '加班补助', '员工旅游']", "['前端开发', '全栈开发', 'CSS3', 'HTML5', 'JavaScript', 'Web端', 'JQuery']"], ['爱菩新医药', '曾先生', '广州天河区龙口', '10-13K', "['1-3年', '大专']", '[]', "['vue.js']"], ['广州零加零科技', '张女士', '广州越秀区东湖', '8-13K', "['1-3年', '大专']", "['五险一金', '带薪年假', '定期体检', '零食下午茶', '餐补', '节日福利', '有无线网', '全勤奖', '生日福利']", "['React', 'JQuery', 'Web端', 'Vue', '前端开发']"], ['OneWay', '陈先生', '广州天河区车陂', '10-15K·13薪', "['1-3年', '大专']", "['加班补助', '带薪年假', '定期体检', '零食下午茶', '员工旅游', '股票期权', '餐补', '节日福利', '补充医疗保险', '年终奖', '五险一金']", "['前端开发', 'WEB开发', 'JavaScript', 'Vue', '小程序', 'Web端', 'GIT', 'CSS3']"], ['育智教育科技', '吴先生', '广州天河区猎德', '10-12K', "['1-3年', '大专']", '[]', "['web开发', 'Javascrip', '微信小程序开发', 'vue全家桶']"], ['币云跳动', '陈女士', '广州白云区嘉禾', '7-12K', "['1-3年', '大专']", "['年终奖', '员工旅游', '股票期权', '带薪年假', '零食下午茶', '五险一金', '节日福利']", "['JavaScript', 'CSS', 'CSS3', 'HTML5', 'AJAX', '前端开发', '移动端', 'Vue']"], ['8684', '吴女士', '广 州', '8-13K', "['1-3年', '大专']", "['补充医疗保险', '餐补', '零食下午茶', '定期体检', '节日福利', '全勤奖']", "['前端开发', 'React', 'JQuery', 'Vue', 'Web端', 'Webpack']"], ['秒即科技有限公司', '陈先生', '广州白云区钟落潭', '8-13K', "['1-3年', '大专']", "['住房补贴', '交通补助', '带薪年假', '定期体检', '员工旅游', '包吃', '餐补', '补充医疗保险', '五险一金', '节日福利', '通讯补贴']", "['前端开发', 'Node.js', 'WEB开发', 'HTML', 'CSS', 'JavaScript', 'HTML5', 'ES6']"], ['中科云图', '邱先生', '广州越秀区淘金', '8-13K·13薪', "['1-3年', '大专']", "['五险一金', '带薪年假', '定期体检', '年终奖', '零食下午茶', '员工旅游', '节日福利', '股票期权']", "['HTML', 'CSS', 'CSS3', 'AJAX', '前端开 发', 'Vue', 'React', 'GIT']"], ['无忧物流', '尚女士', '广州天河区棠下', '10-15K', "['1-3年', '大专']", "['餐补', '节日福利', '年终奖', '带薪年假', '定期体检']", "['前端开发', 'WEB开发', 'HTML', 'CSS', 'CSS3', 'HTML5', 'React', '移动端']"], ['英朗信息技术', '梁女士', '广州天河区珠江新城', '8-13K', "['1-3年', '大专']", "['交通补助', '节日福利', '零食下午茶', '餐补', '员工旅游', '补充医疗保险']", "['前端开发', 'HTML', 'CSS', 'JavaScript', 'CSS3', '小程序', 'PC端', 'SVN']"], ['优创', '王先生', '广州天河区珠江新城', '7-12K', "['1-3年', '大专']", "['节日福利', '全勤奖', '年终奖', '股票期权', '加班补助', '零食下午茶', '员工旅游', '五险一金']", "['JavaScript', 'HTML', 'CSS', 'Less', 'ES6', 'MVC开发', '前端开发', 'Flash开发']"], ['广州九跃', '陈先生', '广州天河区龙口', '8-13K·13薪', "['1-3年', '大专']", '[]', "['前端开发', 'iOS', '小说app', '安卓']"], ['广 州指上科技有限公司', '陈先生', '广州天河区东圃', '10-15K·13薪', "['1-3年', '大专']", "['带薪年假', '全勤奖', '交通补助', '零食下午茶', '节日福利', '员工旅游', '升职加薪', '餐补', '五险一金', '团队拓展', '加班餐补', '年终奖']", "['HTML', '小游戏', 'TypeScript', '前端开发', 'layabox', 'unity3D', '游戏引擎']"], ['腾鼎科技', '潘女士', '广州天河区天河公园', '10-15K', "['1-3年', '大专']", "['五险一金', '生日福利', '全勤奖', '员工旅 游', '补充医疗保险', '餐补', '节日福利', '定期体检', '团建聚餐', '零食下午茶']", "['U3D开发', 'Cocos2dx开发', '游戏引擎', 'C#', 'Lua', '移动端', '游戏']"], ['指游互娱', '谢先生', '广州天河区东圃', '7-12K', "['1-3年', '大专']", "['年终奖', '全勤奖', '带薪年假', '员工旅游', '节日福利', '加班补助', '五险一金', '零食下午茶']", "['HTML', '游戏引擎']"], ['广东新邦智联科技公司', '卢先生', '广州', '11-19K', "['1-3年', '大专']", "['五 险一金', '餐补', '节日福利', '零食下午茶']", "['SDK', 'Kotlin', 'AndroidStudio', 'Framework']"], ['竞游', '周先生', '广州天河区龙口', '9-14K', "['1-3年', '大专']", "['五险 一金', '全勤奖', '节日福利', '带薪年假', '交通补助', '零食下午茶', '包吃']", "['C#', '游戏引擎', 'Android', 'UGUI']"], ['店客多', '肖先生', '广州越秀区流花', '13-18K', "['1-3年', '大专']", "['节日福利', '带薪年假', '定期体检', '补充医疗保险', '免费班车', '五险一金', '零食下午茶']", "['JavaScript', 'CSS3', 'HTML5', 'React', 'Web端', 'React.JS', 'Native.js', '移动端']"], ['中数通', '余女士', '广州天河区天河客运站', '8-13K', "['1-3年', '大专']", "['绩效奖金', '企业年金', '生日福利', '年终奖', '带薪年假', '定期体检', '餐补', '补充医疗保险', '节日福利', '员工旅游', '高温补贴', '五险一金', '通讯补贴']", "['JavaScript', 'Web端', 'MySQL', 'Hybrid开发', 'Visual Studio', 'GIT']"], ['织赢科技', '邓先生', '广州天河区跑马场', '9-14K', "['1-3年', '大专']", "['年终奖', '定期体检', '五险一金', '加班补助', '带薪年假', '通讯补贴', '交通补助', '餐补']", "['前端开发', 'CSS', 'HTML', 'HTML5', 'CSS3', 'JQuery', 'Bootstrap', 'Web端']"], ['悦恺', '罗先生', '广州天河区棠下', '7-12K·13薪', "['1-3年', '大专']", '[]', "['前端开发', 'HTML', 'CSS', 'JavaScript', 'ES6', 'React', '移动端']"]]
# # 写入csv 文件中
def write_csv(file,data):
    with open(file,'w',encoding='utf-8',newline='') as f:
        writer = csv.writer(f)
        writer.writerows(data)
        print("已完成写入操作！")


# 调用
# get_html(1)

# 测试
# write_csv('../【4】自己的小练习/文件/boss.csv',alist)


def getcookie():
  url = 'https://www.zhipin.com/wapi/zppassport/get/zpToken?v=1661679281530'
  Hostreferer = {
    'Host': 'www.zhipin.com',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',        'Accept-Language': 'zh-CN,zh;q=0.9',
    'Accept-Encoding': 'gzip, deflate, br'
  }
  #urllib或requests在打开https站点是会验证证书。 简单的处理办法是在get方法中加入verify参数，并设为False
  html = requests.get(url, headers=Hostreferer,verify=False)
  #获取cookie:DZSW_WSYYT_SESSIonID
  print("cs",html.status_code)
  if html.status_code == 200:
    print("cook",html.cookies)
    for cookie in html.cookies:
      print(cookie)

getcookie()