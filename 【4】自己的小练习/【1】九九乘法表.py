# 九九乘法发表(正序)
def zhengxu() :
    n = 1
    str = ''
    while n <= 9 :
        j = 1
        for j in range(1,10):
            if j <= n :
                str += "%d * %d = %d\t" % (j,n,n*j)
        str += '\n'        
        n += 1
    print(str)


# 九九乘法发表(倒序)
def daoxu() :
    n2 = 9
    str2 = ''
    while n2 >= 1 :
        m = 9
        for m in range(1,10) :
            if m <= n2 :
                str2 += "%d * %d = %d\t" % (m,n2,n2*m)
            m -= 1
        str2 += '\n'        
        n2 -= 1
    print(str2)

# 调用
zhengxu()
daoxu()