
from subprocess import list2cmdline
import numpy
a=[12,13,14,15,16]
b=[2,1,3,4,3]

# 方法一：
e=[]
for index,item in enumerate(a):
    d = item+b[index]
    e.append(d)
# print(e)

# 方法二：
c = numpy.array(a) + numpy.array(b)
print("刚开始的c",type(c))
c = list(c)
print("转换后的c",type(c))

# 方法三：
print([x+y for x,y in zip(a,b)])


# d = zip(a,b)  #zip原来是将对象中对应的元素，一个个对应打包成元组
# print(list(d)) #转换为list 查看