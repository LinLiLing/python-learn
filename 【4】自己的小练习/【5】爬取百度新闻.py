#!/usr/bin/python3
#导入request模块
import requests
import re
from bs4 import BeautifulSoup,Comment
import csv

def getUrl(url):
    '''
    爬取百度网页中，右侧的新闻列表，整合为一个二维数组写入csv文件中
    '''
    header = {
        'user-agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.0.0 Safari/537.36'
    }
    html_data = requests.get(url=url,headers=header).text  #请求数据
    soup = BeautifulSoup(html_data,'lxml') #声明BeautifullSoup对象                      
    
    conList = []
    conList.append(['新闻标题','路径'])
    for i in soup.find_all('a',class_="c-font-medium c-color-t opr-toplist1-subtitle_3FULy"):
        conList.append([i.string,url+i['href']])

    with open("./user_info.csv","w",encoding='utf-8') as f: 
        writer = csv.writer(f)  # writer 函数会返回一个writer对象，通过writer对象向csv文件写入数据
        writer.writerows(conList)
    return conList  

# 调用
newsList =  getUrl('https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&tn=baidu&wd=python3%20%E5%AD%A6%E4%B9%A0&oq=python%2526lt%253B%2520web&rsv_pq=8cce347f00091658&rsv_t=13e6%2BF%2Finy%2BLeLGWsxXbxspw%2BrTiZgvKBV%2Flyf5ThPMp0fGVtnMRh1V0tdI&rqlang=cn&rsv_dl=tb&rsv_enter=1&rsv_btype=t&inputT=9078&rsv_sug3=100&rsv_sug1=63&rsv_sug7=101&rsv_sug2=0&rsv_sug4=9078')
print(newsList)