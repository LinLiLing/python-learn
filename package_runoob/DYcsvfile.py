# -*- coding:utf8 -*-
# 开发者：周冬雨
# 微信号：ssm127171
# 更新时间：20220704
# 开发语言：python
# 代码描述：一些针对csv文件读写功能封装


# 自定义csv类
class DyCsv(object):

    # 实例化时加载csv文件
    def __init__(self, csv_path, encoding='utf8'):
        self.path = csv_path
        self.encoding = encoding
    
    # 读取数据
    def read_data(self, tab:str=',') -> list:
        fp = open(file=self.path, mode='r', encoding=self.encoding)
        data = fp.readlines()
        data = [ i.replace('\n', '').split(tab) for i in data ]
        fp.close()
        return data
    
    # 写入数据
    def wirte_data(self, data:list, tab:str=',', cover:bool=True):
        if cover:
            md = 'w'
        else:
            md = 'a'
        fp = open(file=self.path, mode=md, encoding=self.encoding)
        # 自动识别data类型，支持处理['a','b']一维数组和[['a','b'],['c','d']]二维数组数据
        if not isinstance(data[0], list):
            cdata = '{}\n'.format(tab.join(data))
            fp.write(cdata)
        else:
            cdata = [ tab.join(i) for i in data ]
            cdata = '\n'.join(cdata) + '\n'
            fp.write(cdata)
        fp.close()
        return 1


# mcsv = DyCsv('test.csv')
# a = [['1','2','3'], ['4','5','6'], ['7','8','9']]
# b = ['yu','b','c']

# mcsv.wirte_data(b, cover=False)



