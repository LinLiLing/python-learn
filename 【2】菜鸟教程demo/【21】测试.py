# import urllib.robotparser #导入用于解析 robots.txt 文件的模块
# rp = urllib.robotparser.RobotFileParser() #将 RobotFileParser 类 赋值给一个变量
# rp.set_url("http://www.musi-cal.com/robots.txt") #设置 robots.txt 文件的 URL
# rp.read() #读取 robots.txt URL 并将其输入解析器
# rrate = rp.request_rate("*") #以 named tuple RequestRate(requests, seconds) 的形式从 robots.txt 返回 Request-rate 形参的内容。 如果此形参不存在或不适用于指定的 useragent 或者此形参的 robots.txt 条目存在语法错误，则返回 None。
# print(rp.crawl_delay("*"))

# a = [ 1,2,3,4,5 ]
# print(a[:])   # 输出：[1, 2, 3, 4, 5]
# print(a[0:])  # 输出：[1, 2, 3, 4, 5]
# print(a[:100])# 输出：[1, 2, 3, 4, 5]
# print(a[-1:]) # 输出：[5]


# list = ['red', 'green', 'blue', 'yellow', 'white', 'black']
# print( list[-2] )  #反向索引（最后一位为-1，倒数第二位为-2）
# print( list[1:3] ) #从正向索引为1的位置，截取到第三位(包含)
# print(list[1:-2]) # 从第二位开始（包含）截取到倒数第二位（不包含）
# list[2] = 2001
# print(list[1:]) #去第二个元素开始后的所有元素 
# print("替换第三个元素：",list)

# list.append('Baidu')
# print("往列表最后插入一个元素：",list)

# del list[1]
# print ("删除第二个元素 : ", list)
# print("长度",len(list))

# print("重复：",['Hi!'] * 4)
# print("拼接：",[1,2,3]+[4,5])
# print("存在：",3 in [1,2,3])
# for x in [1,2,3]: print(x,end="")  #迭代,print(x,end="")代表不换行输出

# squares = [1, 4, 9, 16, 25]
# squares += [36, 49, 64, 81, 100]
# print(squares)

# a = ['a','b','c']
# n = [1,2,3]
# x = [a,n]
# print(x)
# print(a+n)
# print(x[0][1])

# 列表比较
# 导入 operator 模块，是python的内置运算符对应的高效率函数
# import operator 
# a = [1,2]
# b = [2,3]
# c = [2,3]
# print("operator.eq(a,b):",operator.eq(a,b))
# print("相加：",operator.add(a,b))
# print("字符串相加：",operator.concat(a,b))

# 元组
# 1.元组所指向的内存中的内容不可变
# tup = ('r','u','n','o','o','b')
# # tup[0] = 'g'
# print("tup：",id(tup))
# tup = (1,2,9)
# print(id(tup))
# print("长度：",len(tup))
# print("最大：",max(tup))

# 字典
# 使用大括号{}来创建空字典
# emptyDict = {}
# print(emptyDict)
# print("长度：",len(emptyDict))
# print("查看类型：",type(emptyDict))

# tinydict = {'Name': 'Runoob', 'Age': 7, 'Class': 'First','Name':'小菜鸟'}
# print("访问字典中值：",tinydict['Name'])
# tinydict['Age'] = 8   #更新Age
# print("查看更新值",tinydict['Age'])
# del tinydict['Class']   #删除键'Class'
# tinydict.clear()        #清空字典
# del tinydict            # 删除字典
# print(tinydict)
# print(tinydict['Name'])


# 集合：集合是无序的
# basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
# a = set ("测试")
# b = set('内容')
# thisset = set(("Google", "Runoob", "Taobao"))
# print(basket)  #去重
# print(a)
# print(b)
# print(thisset)
# set_0 = {"小伍哥","真","是","帅"}
# set_0.pop() #移除随机一个
# set_0.discard('女助理') #移除指定的集合元素，移除一个不存在的不会报错
# set_0.discard('测试')
# set_0.remove('123')
# print(set_0) 

# 列表：是有序的
# list = ['red', 'green', 'blue', 'yellow', 'white', 'black']
# print(list)

# Fibonacci series: 斐波纳契数列
# 两个元素的总和确定了下一个数
# a, b = 0, 1
# while b < 10:
#     print(b)
#     a, b = b, a+b
#     print("a：",a)
#     print("b：",b)
#     print("a+b：",a+b)

# 迭代器
# 1.1
# list = [1,2,3,4]
# it = iter(list) #创建迭代器对象
# print(next(it)) #输出迭代器的下一个元素
# print(next(it))
# print(next(it))
# print(next(it))
# for x in it:
#     print(x,end="")

# 1.2 迭代器
# import sys  # 引入sys模块,是一个从程序外部获取参数的桥梁
# list = [1,2,3,4]
# it = iter(list) #创建迭代器对象

# while True:
#     try:
#         print(next(it))
#     except StopIteration:
#         sys.exit()

# import sys 
# a=sys.argv[2: ] #sys.argv[ ]其实就是一个列表，里边的项为用户输入的参数
# print(a)

# a = [66.25, 333, 333, 1, 1234.5]
# print(a.count(333),a.count(66.25))
# a.insert(2,-1)
# print(a)
# a.append("测试")
# print(a)

# a = [1,2,4]
# b = ['x','c','v']
# print(dict(zip(b,a)))

# 将列表当做堆栈使用
# stack  = [3,4,5]
# stack.append(6)
# print(stack)
# print(stack.pop())
# print(stack.pop())

# 列表当作队列使用
# queue = deque(['a','b','c'])
# queue.append('d')
# queue.append('e')
# print(queue.popleft()) #取出最左边的一个值
# print(queue.popleft())
# print(queue)

# 列表推导式
# vec = [2,3,6]
# print([3*x for x in vec])
# print([[x,3*x] for x in vec])
# freshfruit = ['  banana', '  loganberry ', 'passion fruit  ']
# print([weapon.strip() for weapon in freshfruit])
# print(3*x for x in vec if x>3)

# vec1 = [2, 4, 6]
# vec2 = [4, 3, -9]
# print([x*y for x in vec1 for y in vec2])
# print([vec1[i]*vec2[i] for i in range(len(vec1))])
# print([round(355/113,i) for i in range(1,6)]) # round()方法返回浮点数x的四舍五入值
# print(f'我的成绩为{[round(355/113,i) for i in range(1,6)]}')



# match.celling
# // 整除
# %f 字符串截断


# value = 2.355
# print("%.2f" % value)

# round() 方法：返回浮点数x的四舍五入，对精度要去不高可以用，不然尽量避免使用
# 出现问题描述：
# print(round(2.355, 2))  #实际输出：2.35  ===>  预想得到的是：2.36
# print(round(0.5,1))     #实际输出：0.5   ===>  预想想得到的是：1.0



# # 精确保留小数点后几位（四舍五入）方法（list--方式）
# # num: 用户输入的数值
# # digit：保留的位数
# def num_format(num,digit): 
#     try:                                                # 异常捕获：当用户输入整数的时候，执行except
#         floatVal = str(num).split('.')[1]               # 截取小数点后的数字
#         intVal = str(num).split('.')[0]                 # 截取整数部分
#     except:
#         floatVal = '0'
#         intVal = num

#     if len(floatVal) > digit:                            # 小数点后位数 > 要保留的位数
#         lst = list(floatVal)                             # 转换为列表，赋值给变量
#         x = 1 if (int(floatVal[digit]) >= 5)  else 0     # 判断是否需要进1，赋值给变量
#         lst[digit-1] = str(int(lst[digit-1]) + x)        # 保留的位数
#         floatVal = ''.join(lst[0:digit])                 # 截取小数点0位开始 ———— 保留位数的长度
#     elif (len(floatVal) < digit) and len(floatVal) != 0: # 小数点后位数 < 要保留的位数，并且不等于0
#         lst0 = list(floatVal)       
#         lst1 = ['0']*digit                               # 以要保存的位数为参数，定义一个全 0 的列表。用于截取已存在的位数到要保存的位数。
#         floatVal = ''.join(lst0 + lst1[len(floatVal)-1:-1] ) # 拼接两个列表，并转换为string

#     return f'{intVal}.{floatVal}'  # 返回格式化的值

# # 打印验证
# print(num_format(97987,2))  
# print(num_format(1.345,2))  
# print(num_format(1.455,2))  
# print(num_format(89.34556,3))  



# # 精确保留小数点后几位（四舍五入）方法（list--方式）
# # num: 用户输入的数值
# # digit：保留的位数
# def num_format(num,digit): 
#     try:                                                # 异常捕获：当用户输入整数的时候，执行except
#         floatVal = str(num).split('.')[1]               # 截取小数点后的数字
#         intVal = str(num).split('.')[0]                 # 截取整数部分
#     except:
#         floatVal = '0'
#         intVal = num

#     if len(floatVal) > digit:                            # 小数点后位数 > 要保留的位数
#         # lst = list(floatVal)                             # 转换为列表，赋值给变量
#         x = 1 if (int(floatVal[digit]) >= 5)  else 0     # 判断是否需要进1，赋值给变量
#         floatVal[digit-1] = int(floatVal[digit-1]) + x        # 保留的位数
#         floatVal = floatVal[0:digit]                 # 截取小数点0位开始 ———— 保留位数的长度
#     elif (len(floatVal) < digit) and len(floatVal) != 0: # 小数点后位数 < 要保留的位数，并且不等于0
#         lst0 = list(floatVal)       
#         lst1 = ['0']*digit                               # 以要保存的位数为参数，定义一个全 0 的列表。用于截取已存在的位数到要保存的位数。
#         floatVal = ''.join(lst0 + lst1[len(floatVal)-1:-1] ) # 拼接两个列表，并转换为string

#     return f'{intVal}.{floatVal}'  # 返回格式化的值

# # 打印验证
# print(num_format(97987,2))  
# print(num_format(1.345,2))  
# print(num_format(1.455,2))  
# print(num_format(89.34556,3))  


# a = '1.3453'
# a[3] = '9'
# print(a)
# print(a[0:3])

# b = [1,2,3,4]
# b[3] = 9
# print(b)

# c = 2
# c *= 3

# print(c is not b) # is not身份运算符  输出：True
# print(2*3)  # 算术运算符    输出：6
# print(c = 6)  # 赋值运算符  输出： TypeError: 'c' is an invalid keyword argument for print() "TypeError：“c”是print（）的无效关键字参数"
#               # 为什么要把运算符放进去？ ==想着方便输出，预想输出：6

# print('12'.zfill(5)) 
# import math
# print('常量pi的值：{0:.3f}'.format(math.pi))
# print('常量 PI 的值近似为：%5.3f。' % math.pi)

# hello = 'hello, runoob\n'
# print(repr(hello)) # 可以转义字符串中的字符串
# print(str(hello))

# 文件操作
# 打开一个文件
# f = open("./myweb/foo.txt", "rb+")
# # num = f.write( "Python 是一个非常好的语言。\n是的，的确非常好!!\n" )
# # print(num)
# f.write(b'0123456789abcdef')
# print(f.seek(5))
# print(f.seek(-3, 2)) # 移动到文件的倒数第三字节

# f = open("D:/Ling_GF_Project/python-learn/myweb/foo.txt",'r+')
# str = f.readline() # 只输出一行
# print(str)
# for line in f:    # 循环取出行数据
#     print(line,end='')

# f.close()   # 关闭打开的文件


# import json
# names = ['Jack', 'Dennis', 'Ritchie', 'Mike', 'Tony']
# # dumps 传入一个对象，转换为一个JSON字符串
# result = json.loads(names)
# print(result)
# print(type(result))


# import os
# # 假定 /tmp/foo.txt 文件存在，并有读写权限
# ret = os.access("D:/Ling_GF_Project/python-learn/myweb/foo.txt", os.R_OK)
# print ("F_OK - 返回值 %s"% ret)


# x = 10
# if x > 5:
#     raise Exception('x 不能大于 5。x 的值为: {}'.format(x))


# 类是用来描述具有相同的属性和方法的对象集合。它定义了该集合中每个对象所共有的属性和方法。对象是类的实例。
# __init__()是类的一个特殊的构造方法，该方法在实例化的时候会自动调用
# class MyClass:
#     """一个简单的类实例"""
#     i = 12345
#     def f(self):
#         return 'hello world'
#     def __init__(self, realpart, imagpart):
#         self.r = realpart
#         self.y = imagpart

# # 实例化类
# x = MyClass(1,2)

# # 访问类的属性和方法
# print("MyClass 类的属性 i 为：", x.i)
# print("MyClass 类的方法 f 输出为：", x.f())
# print(f"__init__()方法测试：{x.r}---{x.y}")

# 【1】单继承类
# class people:
#     # 定义基本属性
#     name = ''
#     age = 0
#     # 定义私有属性，私有属性在类外部无法直接访问
#     __weight = 0
#     # 定义构造方法
#     def __init__(self,n,a,w):
#         self.name = n
#         self.age = a
#         self.__weight = w
#     def speak(self):
#         print("%s 说: 我 %d 岁。" %(self.name,self.age))
# # 单继承示例
# class student(people):
#     grade = ''
#     def __init__(self, n, a, w, g):
#         # 调用父类的构造
#         people.__init__(self, n, a, w)
#         self.grade = g
#     # 覆写父亲的方法
#     def speak(self):
#         print("%s 说: 我 %d 岁了，我在读 %d 年级"%(self.name,self.age,self.grade))
# # s = student('ken',10,60,3)
# # s.speak()

# # 另一个类，多重继承之前的准备
# class speaker():
#     topic = ''
#     name = ''
#     def __init__(self,n,t):
#         self.name = n
#         self.topic = t
#     def speak(self):
#         print("我叫 %s，我是一个演说家，我演讲的主题是 %s"%(self.name,self.topic))
# # 多重继承
# class sample(speaker,student):
#     a = ''
#     def __init__(self, n, a, w, g,t):
#         student.__init__(self,n,a,w,g)
#         speaker.__init__(self,n,t)
# test = sample("Tim",25,80,4,'python')
# test.speak()

# 【2】方法重写
# class Parent():   # 定义父类
#     def myMethod(self):
#         print('调用父类方法')
# class Child(Parent): #定义子类
#     def myMethod(self):
#         print('调用子类的方法')
# c = Child()   # 子类实例
# c.myMethod()  # 子类调用重写方法
# # super(Child,c).myMethod() # 用子类对象调用父类已被覆盖的方法
# # print(Child.__mro__)  #可以利用类的属性__mro__ 来查看一个类的继承顺序
# print(type(c))


# 广度优先遍历
# class D():
#     pass
# class C(D):
#     pass
# class B(C):
#     pass
# class A(B,C):
#     pass
# print(A.__mro__)  # 顺序：A => B => C => D
# print(type('trsr'))

# # 深度优先遍历
# class D():
#     pass
# class E():
#     pass
# class C(E):
#     pass
# class B(D):
#     pass
# class A(B,C):
#     pass
# print(A.__mro__)  # 顺序：A => B => D => C => E

# super函数也不是直接调用父类的方法，它是用了mro顺序里面调用下一个类的方法
# class A():
#     def __init__(self):
#         print("A")
# class B(A):
#     def __init__(self):
#         print("B")
#         super().__init__()
# class C(A):
#     def __init__(self):
#         print("C")
#         super().__init__()
# class D(B,C):
#     def __init__(self):
#         print("D")
#         super().__init__()
# d = D() # super函数和父类没有直接关联，它和IMRO查找顺序有关

# __init__:构造函数，在生成对象时调用
# class Father(object):
#     def __init__(self,name):
#         self.name = name
#         print("name: %s" % (self.name))
#     def getName(self):
#         return 'Father ' + self.name
# class Son(Father):
#     def getName(self):
#         return 'Son ' + self.name
#     def __del__()
# if __name__ == '__main__':  # __main__：自己调用自己。所以：if__name__ =' __main__' 就可以判断是否是模块自己运行
#     son = Son('runoob')
#     print(son.getName)

# print(__name__)


# class Vector:
#     def __init__(self,a,b):
#         self.a = a
#         self.b = b
#     def __str__(self):
#         return 'Vector (%d,%d)' % (self.a, self.b)
#     def __add__(self,other):
#         return Vector(self.a + other.a, self.b + other.b)
# v1 = Vector(2,10)
# v2 = Vector(5,-2)
# print(v1 + v2)


# os
import os
print('当前路径：',os.getcwd())     #返回当前的工作目录
print('修改当前的工作目录：',os.chdir('/server/accesslogs')) #修改当前的工作目录