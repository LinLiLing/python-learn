# # 导入模块
# # 导入特定模块，不导入全部 => from ... import ...
# from mode_test import print_func,test
# # 调用模块里的函数
# print_func("小冰")
# test("xiaojf")


#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
############# 作用域与命名空间 ###############################
# Money = 2000
# def AddMoney():
#    # 告诉python => MOney是一个全局变量
#    global Money
#    Money = Money + 1

# print(Money)
# AddMoney()
# print(Money)


############ dir() 函数 #####################################
# 导入内置math模块
import math
content = dir(math)
print(content)