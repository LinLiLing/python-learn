# -*- coding:utf8 -*-
# import webbrowser
# webbrowser.open("http://www.baidu.com")
# webbrowser.open_new("http://www.baidu.com")
# webbrowser.open_new_tab("http://www.baidu.com") # 打开一个新标签页

import webbrowser
import requests
kw = input('百度一下：')
url = 'https://www.baidu.com/s?wd=' + kw
# UA伪装：是指在Http请求中添加User-agent,伪装成浏览器的请求，网络
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:92.0) Gecko/20100101 Firefox/92.0',}
response = requests.get(url=url, headers=headers)
# 将响应内容写入html文件,就获取到百度的内容
fileName = 'a.html'
with open(fileName, 'w', encoding='utf-8') as fp:
    fp.write(response.text)
# 使用浏览器打开页面
webbrowser.open(fileName)




