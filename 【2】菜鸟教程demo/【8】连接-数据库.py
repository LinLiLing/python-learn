#!/usr/bin/python
# -*- coding: UTF-8 -*-

# pymysql 是python3.x 版本中连接mysql的一个库，python2则使用MySQLdb
import pymysql

# 打开数据库连接
db = pymysql.connect(host='localhost',user='root',password='root',database='testdb')

# 使用 cursor() 方法创建一个游标对象cursor
cursor = db.cursor()

# 使用 execute() 方法执行 SQL 查询
cursor.execute("select version()")

# 使用 fetchone() 方法获取单条数据
data = cursor.fetchone()

print("Database version : %s " % data)

# 关闭数据库连接
db.close()




