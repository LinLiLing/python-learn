# 导入urllib库爬取网页
import urllib.request

# 将爬取到的内容赋值给一个变量
# file = urllib.request.urlopen("http://www.baidu.com")
file = urllib.request.urlopen("http://blog.csdn.net/qq_57756904/article/details/125221135?spm=1001.2100.3001.7377&utm_medium=distribute.pc_feed_blog_category.none-task-blog-classify_tag-1-125221135-null-null.nonecase&depth_1-utm_source=distribute.pc_feed_blog_category.none-task-blog-classify_tag-1-125221135-null-null.nonecase")

# 将网页的内容读取出来（读取全部 => 为一个string字符串）
data = file.read()
# print(data)

# 读取文件的一行内容
# dataline = file.readline()
# print(dataline)

# 读取文件的全部内容，赋值给一个列表变量,若要读取全部建议使用这种方式。
# datalines = file.readlines()
# print(datalines)

# 将获取的网页保存到本地文件中(myweb文件中)
fhandle = open("E:/git-project/python-learn/myweb/1.html","wb")
fhandle.write(data)
fhandle.close()

# **使用urlretrieve() 直接将对应信息写入本地 *****************************************
# filename = urllib.request.urlretrieve("http://www.baidu.com",filename="E:/git-project/python-learn/myweb/2.html")

# 清除 urlretrieve 执行过程中会产生很多缓存
# urllib.request.urlcleanup()

# 返回与当前环境有关的信息
# print(file.info())

# 获取当前爬取网页的状态码
# print(file.getcode())

# 获取当前所爬取的url
# print(file.geturl())

# 【编码】对url进行编码，在url中输入例如中文或者“：”或者“&”等不符合（ASCII）标准的字符时，需要进行编码
# print(urllib.request.quote("http://www.sina.com.cn"))

# 【解码】对于有些时候需要对url进行解码
# print(urllib.request.unquote("http%3A//www.sina.com.cn"))