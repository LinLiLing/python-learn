import pymysql

# 打开数据库连接
db = pymysql.connect(host='localhost',user='root',password='root',database='testdb')

# 使用cursor() 方法获取操作游标
cursor = db.cursor()

# SQL更新查询
sql = "update employee set age = age + 1 where sex = '%s'" % ('男')
try:
    # 执行SQL语句
    cursor.execute(sql)
    # 提交到数据库执行
    db.commit()
except:
    # 发生错误时回滚
    db.rollback()

# 关闭数据库连接
db.close()