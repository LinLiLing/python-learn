import pymysql

# 打开数据库连接
db = pymysql.connect(host="localhost",user="root",password="root",database="testdb")

# 使用cursor()方法操作游标
cursor = db.cursor()

# SQL 删除语句
sql ="delete from employee where age > %s" % (20)
try:
    # 执行SQL语句
    cursor.execute(sql)
    # 提交修改
    db.commit()
except:
    # 发生错误时候回滚
    db.rollback()
# 关闭连接
db.close()