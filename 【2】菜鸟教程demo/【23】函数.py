# 修改全局变量 num：global
num = 1
def test1():
    global num  #定义一个函数外部可访问的关键字声明
    print(num)
    num = 123
test1()
print(num)

# 修改嵌套作用域: 修改嵌套作用域（enclosing 作用域，外层非全局作用域）中的变量则需要 nonlocal 关键字
def outer(): 
    num1 = 10
    def inner():
        nonlocal num1  #关键字
        num = 100
        print(num1)
    inner()
    print(num1)
outer()