
# 循环 ################################
# import numbers
# numbers = []
# for number in range(10):
#     numbers.append(number)
# print(numbers)

# map()对象 ###########################
# vat_percent = 0.1 # 10%
# def add_vat(price):
#     return price + (price * vat_percent)
# prices = [10.03,8.6,32.85,41.5,22.64]
# grand_prices = map(add_vat,prices)
# print(grand_prices)
# grand_prices =list(grand_prices)
# print(grand_prices)

# 获取1-100中的偶数
# 方法1 - 正常情况下的方式
# list = []
# for i in range(1,101): #循环从1开始到101的整数序列组 
#     if i%2 == 0: #判断i对2进行取余是否等于0
#         list.append(i)  #等于0执行：将i写入到list中
# print(list)

# 方法2 - 用列表解析式
# list = [i for i in range(1,101) if i%2==0]
# print(list)

# 例1：取文本中所有单词的第一个字符
# words = "you need python"
# first_char = [word[0] for word in words.split()]
# print(first_char)

# 例2，获取两个列表对应位的乘积
# lst1 = [2,3,4]
# lst2 = [5,6,7]
# list3 = [i*j for i,j in zip(lst1,lst2)] #zip函数用于将可迭代的对象作为参数，将对象中对应的元素打包成一个个元组，然后返回这些元组组成的对象，这样做的好处是节约了不少内存。
# print(list3)

# 例3：带三元表达式的列表解析
# 需求：将列表中可以转换为数字的字符串转换为对应的数字，不能转换的内容则修改为None
# list = ['99','18','q','%','20%']
# list = [int(i) if i.isdigit() else None for i in list] #isdigit() 方法检测字符串是否只由数字组成，只对0和正数有效
# print(list)
# # 输出：[99, 18, None, None, None]

# 例4：获取一个全0列表
# 需求：有时候我们可能需要获取一个长度指定，全零或者全为某个值的列表
# lst = ["ok" for i in range(5)]
# print(lst)

# 例5，嵌套循环的列表解析
# 将二维列表中的元素抽取到一维列表中
# lsts = [[1,2,3],[4,5],[7,8,9]]
# lst = [i for lst in lsts for i in lst]
# print(lst)
# # 输出：[1, 2, 3, 4, 5, 7, 8, 9]

# 例6，笛卡尔积
# lst1 = ["ming","ning","tian"]
# lst2 = ["love","like","at"]
# lst3 = ["top","mid","bot"]
# lst = [{"player":x,"action":y,"position":z} for x in lst1 for y in lst2 for z in lst3]
# print(lst)
# # # 输出：[{'player': 'ming', 'action': 'love', 'position': 'top'}, {'player': 'ming', 'action': 'love', 'position': 'mid'}, {'player': 'ming', 'action': 'love', 'position': 'bot'}, {'player': 'ming', 'action': 'like', 'position': 'top'}, {'player': 'ming', 'action': 'like', 'position': 'mid'}, {'player': 'ming', 'action': 'like', 'position': 'bot'}, {'player': 'ming', 'action': 'at', 'position': 'top'}, {'player': 'ming', 'action': 'at', 'position': 'mid'}, {'player': 'ming', 'action': 'at', 'position': 'bot'}, {'player': 'ning', 'action': 'love', 'position': 'top'}, {'player': 'ning', 'action': 'love', 'position': 'mid'}, {'player': 'ning', 'action': 'love', 'position': 'bot'}, {'player': 'ning', 'action': 'like', 'position': 'top'}, {'player': 'ning', 'action': 'like', 'position': 'mid'}, {'player': 'ning', 'action': 'like', 'position': 'bot'}, {'player': 'ning', 'action': 'at', 'position': 'top'}, {'player': 'ning', 'action': 'at',
# 'position': 'mid'}, {'player': 'ning', 'action': 'at', 'position': 'bot'}, {'player': 'tian',
# 'action': 'love', 'position': 'top'}, {'player': 'tian', 'action': 'love', 'position': 'mid'}, {'player': 'tian', 'action': 'love', 'position': 'bot'}, {'player': 'tian', 'action': 'like', 'position': 'top'}, {'player': 'tian', 'action': 'like', 'position': 'mid'}, {'player': 'tian', 'action': 'like', 'position': 'bot'}, {'player': 'tian', 'action': 'at', 'position': 'top'}, {'player': 'tian', 'action': 'at', 'position': 'mid'}, {'player': 'tian', 'action': 'at', 'position': 'bot'}]

# 例7，多值的列表解析
# D = {
#     'one':1,
#     'two':2,
#     'three':3,
#     'four':4,
#     'five':5
# }
# lst = [f'{key} = {value}' for (key,value) in D.items()]
# print(lst)
# # 输出：dict_items([('one', 1), ('two', 2), ('three', 3), ('four', 4), ('five', 5)])

# 例8，列表解析打印内容
lst = [1,2,3,4]
[print(i) for i in lst]