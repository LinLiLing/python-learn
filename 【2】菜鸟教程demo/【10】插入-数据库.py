from colorama import Cursor
import pymysql

# 打开数据库连接
db = pymysql.connect(host="localhost",user="root",password="root",database="testdb")

# 使用 cursor() 方法获取操作游标
cursor = db.cursor()

# SQL 插入语句
# sql  = """insert into employee( first_name,
#           last_name,age,sex,income)
#           values('Mac','Mohan',20,'男',2000)"""

# 或者写成
sql = "insert into employee(first_name,\
       last_name,age,sex,income) \
       values('%s', '%s', '%s', '%s', '%s')" % \
       ('小米','李',21,'男', 3000)

try:
    # 执行sql语句
    cursor.execute(sql)
    # 提交到数据库执行
    db.commit()
except:
    # 如果发生错误则回滚
    db.rollback()

# 关闭数据库
db.close()