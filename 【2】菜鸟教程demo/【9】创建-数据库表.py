from colorama import Cursor
import pymysql

# 打开数据库连接
db = pymysql.connect(host="localhost",user="root",password="root",database="testdb")

# 使用 cursor() 方法创建一个游标
cursor = db.cursor()

# 使用 execute() 方法执行SQL，如果表纯在则删除
cursor.execute("drop table if exists employee")

# 使用预处理语句创建表
sql = """create table employee (
        first_name char(20) not null,
        last_name char(20),
        age int,
        sex char(1),
        income float )"""

cursor.execute(sql)

# 关闭数据库连接
db.close()