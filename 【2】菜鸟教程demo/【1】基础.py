# 【1】整型 int
# a=134

# 【2】浮点型 float
# 指数型式：3.1E5 = 3.1*10^5 =310000
# print(type(3.1E5))
# print(3.1E5)

# 【3】布尔值 True Flase
# print(True)
# print(type(True))
# print(True+2)  # True  对应的是1
# print(False+0) # False 对应是0

# 【4】复数 complex
# z = a+bj  a是实数  b是虚数  j是单位
# print(type(13+4j))
# height = 1.75
# weight = 75.0

# 【5】数值类型
# 数值类型：Number  => int float complex
# 字符串：str  => 单引号、双引号、三引号（也可用多行注释，可以三对单引号或者双引号）
# 列表
# 元组
# 集合
# 字典

# 【6】字符串  
# print("hello\"w\"old")  #输出：hello"w"old  \为转义符号

# 【6-1】 +  字符串拼接 
# a = 'hello'
# b = 'world'
# print(a+' '+b)

# 【6-2】 * 复制：重复输出
# a = 'hello'
# print(a*2)

# 【6-3】in not in  在  不在  里面   
# print('h' in a)     #字符串h在a里面
# print('h' not in a) #字符串h不在a里面

# 【6-4】r\R  原样子输出后面的内容
# print('hello\nhello')
# print(r'\n')   #换行符

# 【小练习 6-4】输出：hello***world 两行
# a= 'hello'
# b= 'world'
# print((a+'***'+b+'\n')*2)

# 【7】格式化输出：按照我们自己想要输出的样式进行输出
# 先定义模板，然后照着模板把变量天进去
# 格式化输出：print(模板 % 变量名)

# 7-1） %s 字符串形式输出
# age = 18
# name = 'LinLing'
# print('我的名字是%s,年龄是%d' % (name,age))

# 7-2) %f 浮点数
# a = 1.332332
# print('%.2f' % a)  #保留2位小数

# 7-3）%d 十进制 
# num = 10.43
# print('数字是：%d' % num)

# 7-4）%0 八进制，满8进1
# num = 9
# print('数字是：%o' % num)  #输出11

# 7-5）其他的%
    # %x 十六进制整数（小写字母ox)
    # %X 十六进制整数（大写字母OX)
    # %e 科学计数法（小写字母'e')
    # %E 科学计数法（大写字母'E')
    # %g %和%e的简写
    # %G %f和%E的简写

# 7-6）format() --- 不带编号
# print('{},{}'.format(12,33))
# a = 'hello'
# b = 'world'
# print('{} {}'.format(b,a))

# 7-6）format() --- 带数字编号，即{1},{2}
# print('{0} {1}'.format('hello','world'))
# print('{0} {1} {0}'.format('hello','world')) #第一个元素为0，第二个元素为1

# 7-6）format() --- 设置参数
# print('网址名:{name},地址：{url}'.format(name='百度',url='www.baidu.com'))

# 7-7）f'{表达式} 格式输出  也不需要考虑数据
# print(f'名字是：{"we"},年龄是：{20}')
# name = 'TOM'
# age = 19
# print(f'名字是：{name},年龄是：{age}')

# 【8】转义字符之换行和制表符
# 转义：默认特殊意义
# \n :换行
# info = '你的名字\n我的名字'
# print(info)

# \t :制表符
# str1 = '网站\t\t\t域名\t\t\t\t年龄\t\t价值'
# str2 = '百度\t\twww.baid.com\t\t\t\t8\t\t5000w'
# print(str1)
# print(str2)

# \\ :反斜杠
# \' :单引号
# print('we\'re friend')  #字符串内用引号，需要转义
# print('we\\er')  # \转义符 \\取消转义，原样输出\
# print('he\tllo')
# print('he\"llo') #he"llo 

# 【9】print() 默认自带\n
# print('输出的内容',end='\n') # end 结束值
# print(123,end="") # 取消换行
# print(234)

# 【10】算术运算符
#  +加   -减   *乘   /除    //取整数    %取余数     ** 幂
# print(10+20)  #加
# print(10-20)  #减
# print(10*20)  #乘
# print(10/20)  #除
# print('-'*50)
# print(10//20) #取整数 
# print(9%2)    #取余数
# print(2**3)   #幂次 8
# print('*'*50)
# print(2+3*5)    #17

# 【11】复合赋值运算符
# = 简单赋值运算符， += 加法赋值运算符， -= 减法赋值运算符， *= 乘法赋值运算符， /= 除法赋值运算符， 
# //= 取整数赋值运算符， %= 取模(余数)赋值运算符， **= 幂赋值运算符
# a = 10
# print(a)

# 多个变量赋值
# num1,float1,str1 = 19,2.3,'hello'
# print(num1)
a=100

# 11-1) +=
# a += 1   # 等于 a=a+1
# print(a)

# 11-1) -=
# a -= 1   # a=a-1
# print(a)

# 11-1) *=
# a *= 2   # a=a*2
# print(a)

# 11-1) /=
# a /= 2   # a=a/2
# print(a)

# 11-1) //=
# a //= 2   # a=a/2
# print(a)

# 11-1) **=
# a **= 2   # a=a**2
# print(a)

# 11-1) %=
# a %= 2   # a=a%2
# print(a)


# 【12】数值类型：int float  布尔序列（True，False)  compile
# 数据结构类型：str  list dict truple
# 类型转换：
# int(x[,base])  将x转换为一个整数
# float(x)       将x转换为一个浮点数
# complex(real[,img]) 创建一个复数，real为实部，imag为虚部
# str(x)         将对象x转换为字符串
# repr(x)        将对象x转换为表达式字符串
# eval(str)      用来计算的字符串中的有效Python表达式并返回一个对象
# truple(s)      将序列s转换为一个元组
# list(s)        将序列s转换为一个列表
# chr(x)         将一个整数转换为一个Unicode字符
# ord(x)         将一个字符转换为它的ASCII整数值
# hex(x)         将一个整数转换为一个十六进制字符串
# oct(x)         将一个整数转换为一个八进制字符串
# bin(x)         将一个整数转换为一个二进制字符串

# a = '123'
# print(int('34'))
# print(float('57.99'))
# print(repr(a))
# print(type(a))


# 【13】input() 输入函数  print()输出
# 输入特点：当程序执行到input,等待用户输入，输入完成之后才继续向下执行
# input 接收用户输入之后，一般存储到变量，方便使用
# input 会把接收到用户输入的数据当做字符串处理
# name = input('请输入：')
# print(name)
# print(type(name))


# 【14】如何限制用户输入
# x = int(input('请输入一个整数：'))   #限制用户输入整数类型
# print(x)

# x = float(input('请输入一个整数：'))   #限制用户输入浮点类型
# print(x)

# x = eval(input('请输入一个整数：'))   #限制得到的数据为数字
# print(x)


# 【15】if 判断基本格式
# if True:    # 代表条件成立
#     print('条件成立执行的代码1')     # 不仅仅是print输出，代表一行执行代码
#     print('条件成立执行的代码2')
# print('我是无论条件是否会执行的代码')

# 【16】True和 False 的界定
# 1.任何非零和非空对象，解释为True
# 2.数字0、空对象、None对象均为假，解释为False
# 3.判断的返回值为True或False
# a = True
# b = 5
# print(a+b)
# print(not 0)

# 【17】if  eles
# # 1.第一个年龄变量
# age = int(input('今年多大了？ '))   

# # 2.判断年龄是否满足18岁
# if age >= 18:
#     print('可以进入网吧嗨皮')
# else:
#     print('你还没长大，应该回家写作业')

# 【18】比较运算符


