#!/user/bin/python
# -*- coding: UTF-8 -*-

# 普通函数
# total = 0 
# def sum( arg1, arg2):
#     total = arg1+ arg2
#     print ("函数内是局部变量：",total)
#     return total

# sum( 10,20 )
# print ("函数外事变量：",total)

# 类
# class Employee :
#     '所有员工基类'
#     empCount = 0

#     def __init__(self,name,salary) :
#         self.name = name
#         self.salery = salary
#         Employee.empCount += 1
    
#     def displayCount(self):
#         print("Toral Employee %d" % Employee.empCount)

#     def displayEmployee(self) :
#         print("Name:", self.name, ",Salary:", self.salery)

# "创建 Employee类的第一个对象"
# emp1 = Employee("Zare",200)
# "创建 Employee类的第二个对象"
# emp2 = Employee("Manni",5000)
# emp1.displayEmployee()
# emp2.displayEmployee()
# print("Total Employee %d" % Employee.empCount)
# emp1.age = 7
# print(hasattr(emp1,'age'))


import re 
# print(re.match('www','www.runoob.com').span()) # 在开始位置匹配
# print(re.search('com','www.runoob.Com',re.I)) # 不在开始位置进行匹配

phone = "2004-98-989 # 这是一个号码"

#  删除字符串中的注释
num = re.sub(r'#.*$',"",phone)
print("号码为：",num)

# 删除非数字(-)的字符串
num = re.sub(r'\D',"",phone)
print("电话为：",num)