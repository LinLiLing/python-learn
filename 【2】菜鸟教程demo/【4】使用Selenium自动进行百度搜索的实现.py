# 库：Selenium 
from datetime import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
 
class Search_Baidu:
    def __init__(self):
        url = 'https://www.baidu.com/'
        self.url = url
 
        options = webdriver.ChromeOptions()
        options.add_experimental_option("prefs", {"profile.managed_default_content_settings.images": 2}) # 不加载图片,加快访问速度
        options.add_experimental_option('excludeSwitches', ['enable-automation']) # 此步骤很重要，设置为开发者模式，防止被各大网站识别出来使用了Selenium
 
        self.browser = webdriver.Chrome(ChromeDriverManager().install(), options=options)
        self.wait = WebDriverWait(self.browser, 10) #超时时长为10s
 
    def search(self, keyword):
        # 打开百度网页
        self.browser.get(self.url)
        # 等待搜索框出现，最多等待10秒，否则报超时错误
        search_input = self.wait.until(EC.presence_of_element_located((By.XPATH, '//*[@]')))
        # 在搜索框输入搜索的关键字
        search_input.send_keys(keyword)
        # 回车
        search_input.send_keys(Keys.ENTER)
        # 等待10秒钟
        self.browser.implicitly_wait(10)
        # 找到所有的搜索结果
        results = self.browser.find_elements_by_css_selector(".c-title .t .t .tts-title")
        # results = self.browser.find_elements_by_css_selector(".t a , em , .c-title-text")
        # 遍历所有的搜索结果
        with open("search_result.txt","w") as file:            
            for result in results:
                if result.get_attribute("href"):
                    print(result.get_attribute("text").strip())
                    # 搜索结果的标题
                    title = result.get_attribute("text").strip()
                    # 搜索结果的网址
                    link = result.get_attribute("href")
                    # 写入文件
                    file.write(f"Title: {title}, link is: {link} \n")
 
    def tear_down(self):
        self.browser.close()
 
if __name__ == "__main__":
    search = Search_Baidu()
    search.search("selenium")
    search.tear_down()