# import os #操作系统接口
# print(os.getcwdb()) #返回当前的工作目录：b'D:\\Ling_GF_Project\\python-learn'

# import sys #命令行参数
# print(sys.argv) #['d:/Ling_GF_Project/python-learn/【2】菜鸟教程demo/【24】标准库概述.py']

# print(sys.stderr.write('Warning,log file not found starting a new one\n')) #错误输出重定向和程序终止

# 【1】正则
import re
# print(re.match('www','www.runoob.com').span()) #(0,3)
# print(re.match('www','dfdwww.runoob.com'))  #None

# line = "Cats are smaeter than dogs"
# matchobj = re.match(r'(.*) are (.*?) .*',line,re.M|re.I)
# print('匹配后的值：',matchobj)
# if matchobj:
#      print ("matchObj.group() : ", matchobj.group())
#      print ("matchObj.group() : ", matchobj.group(1))
#      print ("matchObj.group() : ", matchobj.group(2))
# else:
#    print ("No match!!")

# # 【1.1】sub
# phone = "2004-959-559 #这是一个电话号码"
# #删除注释
# num = re.sub(r'#.*$','',phone)  
# print ("电话号码：",num)
# #移除非数字的内容
# num = re.sub(r'\D',"",phone)
# print("号码：",num)

# 【1.2】repl
# 将匹配的数字乘于 2
# def double(matched):
#     value = int(matched.group('value'))
#     return str(value * 2)
# s = 'A23G4HFD567'
# print(re.sub('(?P<value>\d+)', double, s))  #?P<value>的意思就是命名一个名字为value的组，匹配规则符合后面的\d+

# 【1.2.1】repl    demo
# def pythonReSubDemo():
#     """
#         这个是repl函数的一个使用，
#         demo Python re.sub
#     """
#     inputStr = "hello 123 world 456"

#     def _add111(metched):
#         intStr = metched.group("number")
#         intValue = int(intStr)
#         print(intValue)
#         addedValue = intValue + 111
#         addedValueStr = str(addedValue)
#         return addedValueStr
    
#     replacedStr = re.sub("(?P<number>\d+)",_add111,inputStr)
#     # print("replaceStr=",replacedStr)  #输出：replaceStr= hello 234 world 567

# if __name__ == "__main__":
#     pythonReSubDemo()

# s = 'hello \nworld \nxinfa'
# print(s)
# pattern = re.compile(r'^\w+')
# print(re.findall(pattern,s))   

# 【3】性能度量
# from timeit import Timer
# print(Timer('t=a; a=b; b=t', 'a=1; b=2').timeit())
# print(Timer('a,b = b,a', 'a=1; b=2').timeit())

# 【4】此测试性能
# def average(values):
#     """Computes the arithmetic mean of a list of numbers.

#     >>> print(average([20, 30, 70]))
#     40.0
#     """
#     return sum(values) / len(values)
# import doctest
# doctest.testmod()   # 自动验证嵌入测试


# 【5】时间进度条
# import time
# scale = 50 
# print("执行开始".center(scale//2,"-"))  # .center() 控制输出的样式，宽度为 25//2，即 22，汉字居中，两侧填充 -
# start = time.perf_counter() # 调用一次 perf_counter()，从计算机系统里随机选一个时间点A，计算其距离当前时间点B1有多少秒。当第二次调用该函数时，默认从第一次调用的时间点A算起，距离当前时间点B2有多少秒。两个函数取差，即实现从时间点B1到B2的计时功能。
# for i in range(scale+1):   
#     a = '*' * i             # i 个长度的 * 符号
#     b = '.' * (scale-i)  # scale-i） 个长度的 . 符号。符号 * 和 . 总长度为50 
#     c = (i/scale)*100  # 显示当前进度，百分之多少
#     dur = time.perf_counter() - start    # 计时，计算进度条走到某一百分比的用时
#     print("\r{:^3.0f}%[{}->{}]{:.2f}s".format(c,a,b,dur),end='')  # \r用来在每次输出完成后，将光标移至行首，这样保证进度条始终在同一行输出，即在一行不断刷新的效果；{:^3.0f}，输出格式为居中，占3位，小数点后0位，浮点型数，对应输出的数为c；{}，对应输出的数为a；{}，对应输出的数为b；{:.2f}，输出有两位小数的浮点数，对应输出的数为dur；end=''，用来保证不换行，不加这句默认换行。
#     time.sleep(0.1)     # 在输出下一个百分之几的进度前，停止0.1秒
# print("\n"+"执行结果".center(scale//2,'-'))


# 【6】日期和时间
# import time
# ticks = time.time()
# print("当前时间戳为：",ticks)

# 【7】正则小练习：
# 7-1 
# 要求：把1234567替换成1234567 8910。（如果我们要替换的是原字符串本身或者包换原字符串）
# import re
# content = 'Extra stings Hello 1234567 World_This is a Regex Demo Extra stings'
# content = re.sub('(\d+)',r'\1 8910',content)
# print(content)

# 7-2
# 要求：把所有歌名匹配出来。要用到re.sub内容。因为上一篇我们用re.findall匹配出了所有歌名。当时直接用re.findall写的有些复杂，这次我们练习用re.sub简化下，再re.findall
# 思路：用re.sub把标签替换掉，我们只取所有歌名，标签不需要
html='''<div id="songs-list">
    <h2 class="title">经典老歌</h2>
    <p class="introduction">
        经典老歌列表
    </p>
    <ul id ="list" class="list-group">
        <li data-view="2">一路上有你</li>
        <li data-view="7">
            <a href="/2.mp3" singer="任贤齐">沧海一声笑</a>
        </li>
        <li data-view="4" class="active">
            <a href="/3.mp3" singer="齐秦">往事随风</a>
        </li>
        <li data-view="6"><a href="/4.mp3" singer="beyond">光辉岁月</a></li>
        <li data-view="5"><a href="/5.mp3" singer="陈惠琳">记事本</a></li>
        <li data-view="5">
            <a href="/6.mp3" singer="邓丽君">但愿人长久</a>
        </li>
    </ul>
</div>
'''
html = re.sub('<a.*?>|</a>','',html)
print(html)
results = re.findall('<li.*?>(.*?)</li>',html,re.S)
print(results)
for result in results:
    print(result.strip())

# str = "*****this is **string** example....wow!!!*****"
# print (str.strip( '*' ))  # 指定字符串 *

