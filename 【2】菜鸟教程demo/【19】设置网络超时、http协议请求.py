""" # 设置网络超时
import urllib.request
for i in range(1,10):
    try:
        file = urllib.request.urlopen("https://www.csdn.net/",timeout=30)
        data = file.read()
        print(len(data))
    except Exception as e :
        print("出现异常 -- >" + str(e))
"""

# HTTP 协议请求：Get
# 【1】只能是英文
# import urllib.request
# keywd = "hello"
# url = "http://www.baidu.com/s?wd="+keywd
# req = urllib.request.Request(url)
# data = urllib.request.urlopen(req).read()
# fhandle = open("E:/git-project/python-learn/myweb/4.html","wb")
# fhandle.write(data)
# fhandle.close()

# 【2】优化编码方式
# import urllib.request
# url = "http://www.baidu.com/s?wd="
# key = "微博"
# key_code = urllib.request.quote(key)  #转换编码格式
# url_all = url + key_code
# req = urllib.request.Request(url_all)
# data = urllib.request.urlopen(req,timeout=30).read()
# fh = open("E:/git-project/python-learn/myweb/5.html","wb")
# fh.write(data)
# fh.close()

# Post 请求
import urllib.request
import urllib.parse
url = "http://www.iqianyue.com/mypost/"
postdata = urllib.parse.urlencode({ #编译一个参数
    "name":"ceo@iqianyue.com",
    "pass":"aA123456"
}).encode('utf-8') #将数据使用urlencode编码处理后，使用encode()设置utf-8编码
print(postdata)
# req = urllib.request.Request(url,postdata) #构造request
# req.add_header("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36")
# data = urllib.request.urlopen(req).read() #打开request请求体用一个变量接收
# fhandle = open("E:/git-project/python-learn/myweb/6.html","wb") #在内存里面的文件对象，
# fhandle.write(data) #将data写入这个内存对象
# fhandle.close() #关闭内存里面的文件对象