"""
[课程内容]: Python采集某音乐网站所有榜单音乐

[授课老师]: 青灯教育-自游  [上课时间]: 20:05

[环境使用]:
    Python 3.8
    Pycharm

[模块使用]:
    requests >>> pip install requests
    re
    parsel ---> 数据解析模块

---------------------------------------------------------------------------------------------------
win + R 输入cmd 输入安装命令 pip install 模块名 (如果你觉得安装速度比较慢, 你可以切换国内镜像源)
先听一下歌 等一下后面进来的同学,20:05正式开始讲课 [有什么喜欢听得歌曲 也可以在公屏发一下]
相对应的安装包/安装教程/激活码/使用教程/学习资料/工具插件 可以加落落老师微信
---------------------------------------------------------------------------------------------------
听课建议:
    1. 对于本节课讲解的内容, 有什么不明白的地方 可以直接在公屏上面提问, 具体哪行代码不清楚 具体那个操作不明白
    2. 不要跟着敲代码, 先听懂思路, 课后找落落老师领取录播, 然后再写代码
    3. 不要早退, 课后签到领取福利代码以及课程录播
----------------------------------------------------------------------------------- ----------------
模块安装问题:
    - 如果安装python第三方模块:
        1. win + R 输入 cmd 点击确定, 输入安装命令 pip install 模块名 (pip install requests) 回车
        2. 在pycharm中点击Terminal(终端) 输入安装命令
    - 安装失败原因:
        - 失败一: pip 不是内部命令
            解决方法: 设置环境变量

        - 失败二: 出现大量报红 (read time out)
            解决方法: 因为是网络链接超时,  需要切换镜像源  SSL
                清华：https://pypi.tuna.tsinghua.edu.cn/simple
                阿里云：https://mirrors.aliyun.com/pypi/simple/
                中国科技大学 https://pypi.mirrors.ustc.edu.cn/simple/
                华中理工大学：https://pypi.hustunique.com/
                山东理工大学：https://pypi.sdutlinux.org/
                豆瓣：https://pypi.douban.com/simple/
                例如：pip3 install -i https://pypi.doubanio.com/simple/ 模块名

        - 失败三: cmd里面显示已经安装过了, 或者安装成功了, 但是在pycharm里面还是无法导入
            解决方法: 可能安装了多个python版本 (anaconda 或者 python 安装一个即可) 卸载一个就好
                    或者你pycharm里面python解释器没有设置好
---------------------------------------------------------------------------------------------------
如何配置pycharm里面的python解释器?
    1. 选择file(文件) >>> setting(设置) >>> Project(项目) >>> python interpreter(python解释器)
    2. 点击齿轮, 选择add
    3. 添加python安装路径
---------------------------------------------------------------------------------------------------
pycharm如何安装插件?
    1. 选择file(文件) >>> setting(设置) >>> Plugins(插件)
    2. 点击 Marketplace  输入想要安装的插件名字 比如:翻译插件 输入 translation / 汉化插件 输入 Chinese
    3. 选择相应的插件点击 install(安装) 即可
    4. 安装成功之后 是会弹出 重启pycharm的选项 点击确定, 重启即可生效
---------------------------------------------------------------------------------------------------

零基础同学 0
有基础同学 1  自己有写过爬虫案例

爬一首歌 ---> 爬一个榜单 ---> 爬所有榜单

爬虫基本思路流程: <采集任何网站数据内容, 都可以按照这个流程去走>

一. 数据来源分析:
    1. 明确需求, 找我们找数据源
    2. 通过开发者工具进行抓包分析, 分析我们想要数据从哪里来的
        - 开发者工具 ---> F12 或者鼠标右键点击检查..刷新网页  去找音频播放地址链接
        - 点击media 可以找音频url地址
        - 通过关键字搜索, 找寻音频地址来源

二. 代码实现步骤过程:
    1. 发送请求, 模拟浏览器对于url地址发送请求
    2. 获取数据, 获取服务器返回响应数据
    3. 解析数据, 提取我们想要数据内容
    4. 保存数据, 保存音频到本地

不要企图一节课, 掌握本节课所有内容, 所有知识点内容, 细节, 看录播回放复习


赚钱 ---> 除了免费课程以外, 还是有付费课程  赚钱

我要招生了, 我要带着你们赚钱了, 提供一个更加好, 更加优质课程

广告过后, 精彩继续

包含 核心编程<基础> 高级开发<进阶> 爬虫实战 数据分析 全栈开发<前端+后端> [网站开发]

全套课程学完 7 个月时间 ---> 可以达到什么程度?
    可以直接就业找工作 / 接外包兼职赚钱 8-15K左右薪资工作完全没问题
    爬虫工程师 / 数据分析师 / 开发工程师 <全栈开发> 相关工作都可以从事



想要学习python就业 1
    你就业找工作, 知道面试套路吗, 你有能够支持你面试案例项目吗? 你遇到问题有人给你解答吗?
        你环境没配置 怎么搞呢?

想要学习python兼职 2

需要课程大纲, 学习路线图, 了解系统课程, 了解学习方向, 就业规划, 兼职接外包:
    加婧琪老师微信: python1018

今天报名同学, 是可以享有两年学习权限 ---> 7880 / 2 = 3940
    今天报名可以找婧琪老师微信 预定 300 学费 领取 1000 学费优惠券
        8880-1000-300 = 7580
    - 申请分期免息学习 支付宝花呗, 京东白条, 信用卡,  腾讯课堂官方教育分期渠道
        7580 / 12 = 631/月
        7580 / 18 = 421/月
    0利息0手续费, 并且学费是下个月才支付的
    - 一对一提供外包订单 两个外包 别人花钱请你帮他写程序, 简单外包 别入采集今晚内容 200+
        练手, 让你自己怎么去接外包, 怎么对接, 怎么赚钱变现
    - 提供奖学金, 按时听课, 按时完成作业, 达到学习要求, 可以获得学费20%作为奖学金发给你
        7880 * 20% = 1576
        每个阶段都有  核心编程<基础> 高级开发<进阶> 爬虫实战 数据分析 全栈开发<前端+后端>
        1576 / 6 = 262左右

今天报名:
    第一期学费, 8月20号支付 421 - 200<外包> - 262<奖学金> = -41
    第二期学费, 9月20号支付 421 - 200<外包> - 262<奖学金> = -41
    第三期学费, 10月20号支付 421 - 262<奖学金> 你可以自己已经独立接外包 一个外包可能就是几百 甚至几千都有可能

只要你认真听课学习, 都不会需要担心学费问题

直播授课, 周一 20-22上课, 有什么不懂地方, 可以直接在公屏问
    每节课后都有作业 ---> 本节课所讲解内容知识点 做一个小案例 几个小案例
    写作业不会的地方, 就表示, 你那个知识点没掌握好 --->
        1. 看视频回放 看老师讲知识那块 2. 看课件笔记 3. 找老师解答辅导
   周二全天候让你巩固复习, 给你解答辅导, 让写作业
   周三开课前半个小时左右 19:30 左右会讲解前一节课的作业内容


知识点内容, 你学了后面会使用到前面的知识点, 温故而知新....

独立接外包 0基础正常 3个月 努力型2个月 拼命型1个月  不要命 2周  啥也不要 1周

3-4节课 28节课 500左右随便接


你知道为什么系统课程2-3个小时安排 ---> 精力有限 4-6个小时
    学习一天巩固复习一天 让你写作业 让你们解答辅导

如果是我们的原因, 教学质量不好, 教学服务不好...
我在课堂给你们保障的, 都有是有的, 没有的话申请退学费, 如果没有退的话, 你可以12315工商局举报....


公开课 案例演示, 告诉大家 用python可以做什么 效果是什么样, 为什么这样用
    仅限今晚课程内容
系统课程从零基础入门每个细节都教授, 直接项目实战  是什么 为什么 怎么做 都会教
    会解答辅导 python课程相关内容都会解答 <前端也会解答 全栈开发>

反爬 ---> headers用户代理  验证码 IP代理 JS逆向 字体反爬<找字体文件 映射> APP<换一个抓包工具> 用户模拟登陆....
    混淆换原  cookie加密  ---> 响应头
        cookie ---> 从哪里生成的cookie

    请求五次之后cookie要重新获取, 可以使用selenium 去获取cookie
"""
# 导入数据请求模块  ---> 第三方模块 需要 pip install requests  --> win +R 输入cmd
import requests
# 导入数据格式化模块 ---> 内置模块 不需要安装
import pprint
# 导入正则模块 ---> 内置模块 不需要安装
import re

home_url = 'https://www.kugou.com/yy/html/rank.html'
headers = {
    # user-agent 用户代理, 表示浏览器基本身份标识
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.0.0 Safari/537.36'
}
html_data_1 = requests.get(url=home_url, headers=headers).text
info_list = re.findall('a title="(.*?)".*?hidefocus="true" href="(.*?)" onclick', html_data_1)
# print("榜单集合",info_list[0])
for name, link in info_list[0:2]: #[11:] 韩版需要验证码
    name = re.sub('".*', '', name)
    # print(name, link)   #榜单地址
    # 发送请求获取数据
    # link = 'https://www.kugou.com/yy/rank/home/1-6666.html?from=rank'
    headers = {
        # user-agent 用户代理, 表示浏览器基本身份标识
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.0.0 Safari/537.36'
    }
    # 发送请求 response.text 获取文本数据
    html_data = requests.get(url=link, headers=headers).text
    """
    解析数据 ---> 根据返回数据类型, 以及你想要数据内容, 选择最适合的解析方法
        - 得到是字典数据, 使用字典取值 json解析
        - 得到文本数据 字符串 正则表达式
            re.findall() 通过re<正则模块>里面findall<找到所有>方法去提取数据
                从什么地方去找什么样数据内容
                从 html_data 去找 "Hash":"(.*?)", 这段内容 其中(.*?)就是我们想要的数据
                .*? 正则元字符, 表示可以匹配任意字符 除了换行符\n
    """
    Hash_list = re.findall('"Hash":"(.*?)",', html_data)
    album_id_list = re.findall('"album_id":(.*?),', html_data)
    # 我想要把列表一个一个提取出来, for循环
    for Hash, album_id in zip(Hash_list, album_id_list):
        # print(Hash, album_id)
        """
        1. 发送请求, 模拟浏览器对于url地址发送请求
            - 批量替换方法:
                ctrl + R 输入正则命令
                (.*?): (.*)
                '$1': '$2',
            - 报错: simplejson.errors.JSONDecodeError: Expecting value: line 1 column 1 (char 0)
                不是完整json数据格式, 所以没办法直接得到json字典数据
    
        采集榜单多个音频 ---> 分析请求参数变化规律
        """
        # 确定网址 当我们请求url地址太长的时候, 可以分段写 链接当中问号后面的内容属于请求参数
        url = 'https://wwwapi.kugou.com/yy/index.php'
        # 请求参数 --> 字典数据类型, 构建完整键值对形式 慢一点 1 快一点 2 请求参数不全可以不可以, 不一定
        data = {
            'r': 'play/getdata',
            # 来自于回调
            # 'callback': 'jQuery19107028558321633656_1658319044405',
            'hash': Hash,
            'dfid': '0jILs82Svsxu107QqV4dqfc9',
            'appid': '1014',
            'mid': '79ccf0ae0621ad3b03cebab6d7ef206d',
            'platid': '4',
            'album_id': album_id,
            '_': '1658319044410',
        }
        # 请求头伪装 --> 在开发者工具里面复制的
        headers = {
            # user-agent 用户代理, 表示浏览器基本身份标识
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.0.0 Safari/537.36'
        }
        # 发送请求 <Response [200]> 响应对象, 200状态码表示请求成功
        response = requests.get(url=url, params=data, headers=headers)
        # 获取数据 获取服务器返回响应数据 ---> 把请求参数里面callback删掉, 然后在获取response.json()
        # 格式化输出 更加方便字典取值  pprint.pprint(response.json())
        # 字典取值 ---> 键值对取值, 根据冒号左边的内容[键],提取冒号右边的内容[值]
        title = response.json()['data']['audio_name']
        audio_url = response.json()['data']['play_url']
        title = re.sub(r'[\/:*?"<>|]', '', title)
        if audio_url:
            print(title)
            print(audio_url)
            # 保存数据 ---> 需要发送请求 并且获取数据 response.content 获取二进制数据
            audio_content = requests.get(url=audio_url, headers=headers).content
            with open('D:\\Ling_GF_Project\\python-learn\\【4】自己的小练习\\【4】采集酷狗音乐\\music\\' + title + '.mp3', mode='wb') as f:
                f.write(audio_content)






