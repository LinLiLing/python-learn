# -*- coding:utf8 -*-
# 开发者：周冬雨
# 微信号：ssm127171
# 更新时间：202206015
# 开发语言：python
# 代码描述：爬取国家统计局2021年行政区划数据


# 模块导入
from pydoc import doc
import requests
from bs4 import BeautifulSoup


# 全局变量
BASEURL = 'http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2021/'


# http下载器
def get_html(url):
    html = requests.get(url=url)
    html.encoding = 'utf8'
    document = BeautifulSoup(html.text, 'lxml')
    return document


# html解析器(省级页面),返回全部省份名称和下级连接
def parse_html(document):
    soup = document.select('table.provincetable tr.provincetr a')
    data = [ (i.text, BASEURL + i.get('href')) for i in soup ]
    return data


def parse_html2(document):
    soup = document.select('table.citytable tr.citytr')
    data = [
        ( i.select('a')[1].text, BASEURL + i.select('a')[1].get('href') )
        for i in soup
    ]
    return data

def parse_html3(document):
    soup = document.select('table.countytable tr.countytr')
    data = [
        (i.select('a')[1].text, i.select('a')[1].get('href'))
        for i in soup
    ]
    return data
    




html = get_html(url=BASEURL)
# 获取省名+市区的链接
data = parse_html(html) 
# print(data)
a = 1
for i in data:
    if a > 3:
        break
    html = get_html(url=i[1])
    data = parse_html2(document=html)
    # print(data2)
    for j in data:
        html = get_html(url=j[1])
        data = parse_html3(document=html)
        print(data)
    a += 1

    